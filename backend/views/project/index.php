<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\ProjectSearch $searchModel
 */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a('Create Project', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?php echo GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

//			'id',
			'name',
			'startDate',
			'endDate',
			[
				'attribute' => 'authorId',
				'value' => function ($model, $index, $widget) {
					return $model->author->name;
				}
			],
			'createTime',
			'updateTime',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>

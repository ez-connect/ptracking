<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/**
 * @var yii\web\View $this
 * @var common\models\Project $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="project-form">

	<?php $form = ActiveForm::begin(); ?>

		<?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
		
		<?= $form->field($model, 'code')->textInput(['maxlength' => 8]) ?>

		<?= $form->field($model,'startDate')->widget(
			DatePicker::className(),
			['clientOptions' => ['dateFormat' => 'yy-mm-dd']
		]) ?>

		<?= $form->field($model,'endDate')->widget(
			DatePicker::className(),
			['clientOptions' => ['dateFormat' => 'yy-mm-dd']
		]) ?>

		<div class="form-group">
			<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>

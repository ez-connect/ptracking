<?php
namespace console\controllers;

use yii\console\Controller;
use yii\helpers\Security;

class UserController extends Controller
{
	/**
     * Creates a new Member model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionGenerate($password)
    {
		echo "Create a member\n";

       	echo "Password hash\n";
		echo Security::generatePasswordHash($password) . "\n";
		echo "AuthKey:\n";
		echo Security::generateRandomKey();
    }
}

<?php

use yii\db\Schema;
use yii\db\Migration;

class m150210_063547_create_project extends Migration
{
    public function up()
    {
        // Table options
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('Project', [
            'id'                    => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',
            'name'                  => 'VARCHAR(127) NOT NULL',
            'code'                  => 'VARCHAR(8) NOT NULL',
            'startDate'             => 'DATE NOT NULL',
            'endDate'               => 'DATE NOT NULL',
            'mailinglist'           => 'VARCHAR(64)',
            'foodStoreId'           => 'INT UNSIGNED',
            'status'                => "ENUM('inactive', 'active') DEFAULT 'active'",
            'authorId'              => 'INT UNSIGNED NOT NULL',
            'createTime'            => 'DATETIME',
            'updateTime'            => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ]);

        // Index
        $this->createIndex('IDX_Project_Name', 'Project', 'name', true);
        $this->createIndex('IDX_Project_Code', 'Project', 'code', true);
        $this->createIndex('IDX_Project_Status', 'Project', 'code', false);

        // Foreign key
        $this->addForeignKey('FK_Project_FoodStoreId', 'Project', 'foodStoreId', 'FoodStore', 'id', $delete = 'CASCADE', $update = 'CASCADE');
        $this->addForeignKey('FK_Project_AuthorId', 'Project', 'authorId', 'User', 'id', $delete = 'CASCADE', $update = 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('Project');
    }
}

<?php

use yii\db\Migration;

class m150210_120201_create_reposity extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('Reposity', [
            'id'                    => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',
            'projectId'             => 'INT UNSIGNED NOT NULL',
            'type'                  => "ENUM('Git', 'SVN') DEFAULT 'Git'",
            'url'                   => 'VARCHAR(255) NOT NULL',
            'authorId'              => 'INT UNSIGNED NOT NULL',
            'updateTime'            => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ]);

        // Index
        $this->createIndex('IDX_Reposity_Type', 'Issue', 'status', false);
        $this->createIndex('IDX_Reposity_Url', 'Issue', 'status', true);

        // Foreign key
        $this->addForeignKey('FK_Reposity_ProjectId', 'Reposity', 'projectId', 'Project', 'id', $delete = 'CASCADE', $update = 'CASCADE');
        $this->addForeignKey('FK_Reposity_AuthorId', 'Reposity', 'authorId', 'User', 'id', $delete = 'CASCADE', $update = 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('Reposity');
    }
}

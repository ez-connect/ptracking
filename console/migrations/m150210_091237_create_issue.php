<?php

use yii\db\Schema;
use yii\db\Migration;

class m150210_091237_create_issue extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('Issue', [
            'id'                    => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',
            'projectId'             => 'INT UNSIGNED NOT NULL',
            'versionId'             => 'VARCHAR(32)',
            'type'                  => "ENUM('bug', 'task', 'feedback', 'enhancement', 'proposal') DEFAULT 'bug'",
            'reference'             => 'VARCHAR(255)',
            'priority'              => "ENUM('trivial', 'minor', 'major', 'critical', 'blocker') DEFAULT 'major'",
            'severity'              => 'INT UNSIGNED NOT NULL',
            'priority'              => 'INT UNSIGNED NOT NULL',
            'checklist'             => 'INT UNSIGNED NOT NULL',
            'title'                 => 'VARCHAR(255) NOT NULL',
            'status'                => "ENUM('new', 'on hold', 'resolved', 'open', 'duplicate', 'invalid', 'wont fix', 'spam', 'unresolved', 'closed') DEFAULT 'new'",
            'assigneeId'            => 'INT UNSIGNED',
            'fixerId'               => 'INT UNSIGNED',
            'authorId'              => 'INT UNSIGNED NOT NULL',
            'updateTime'            => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ]);

        // Index
        $this->createIndex('IDX_Issue_Type', 'Issue', 'status', false);
        $this->createIndex('IDX_Issue_Priority', 'Issue', 'status', false);
        $this->createIndex('IDX_Issue_Status', 'Issue', 'status', false);
        $this->createIndex('IDX_Issue_FixerId', 'Issue', 'fixerId', false);
        $this->createIndex('IDX_Issue_AssigneeId', 'Issue', 'assigneeId', false);

        // Foreign key
        $this->addForeignKey('FK_Issue_ProjectId', 'Issue', 'projectId', 'Project', 'id', $delete = 'CASCADE', $update = 'CASCADE');
        $this->addForeignKey('FK_Issue_FixerId', 'Issue', 'fixerId', 'User', 'id', $delete = 'CASCADE', $update = 'CASCADE');
        $this->addForeignKey('FK_Issue_AssigneeId', 'Issue', 'fixerId', 'User', 'id', $delete = 'CASCADE', $update = 'CASCADE');
        $this->addForeignKey('FK_Issue_AuthorId', 'Issue', 'authorId', 'User', 'id', $delete = 'CASCADE', $update = 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('Issue');
    }
}

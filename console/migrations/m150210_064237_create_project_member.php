<?php

use yii\db\Schema;
use yii\db\Migration;

class m150210_064237_create_project_member extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('ProjectMember', [
            'id'                    => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',
            'projectId'             => 'INT UNSIGNED NOT NULL',
            'userId'                => 'INT UNSIGNED NOT NULL',
            'authorId'              => 'INT UNSIGNED NOT NULL',
            'updateTime'            => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ]);

        // Foreign key
        $this->addForeignKey('FK_ProjectMember_ProjectId', 'ProjectMember', 'projectId', 'Project', 'id', $delete = 'CASCADE', $update = 'CASCADE');
        $this->addForeignKey('FK_ProjectMember_UserId', 'ProjectMember', 'userId', 'User', 'id', $delete = 'CASCADE', $update = 'CASCADE');
        $this->addForeignKey('FK_ProjectMember_AuthorId', 'ProjectMember', 'authorId', 'User', 'id', $delete = 'CASCADE', $update = 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('ProjectMember');
    }
}

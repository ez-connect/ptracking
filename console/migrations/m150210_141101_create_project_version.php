<?php

use yii\db\Migration;

class m150210_141101_create_project_version extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('ProjectVersion', [
            'id'                    => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',
            'projectId'            	=> 'INT UNSIGNED NOT NULL',
            'version'               => 'VARCHAR(64) NOT NULL',
            'authorId'              => 'INT UNSIGNED NOT NULL',
            'updateTime'            => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ]);

        // Foreign key
        $this->addForeignKey('FK_ProjectVersion_ProjectId', 'ProjectVersion', 'projectId', 'Project', 'id', $delete = 'CASCADE', $update = 'CASCADE');
        $this->addForeignKey('FK_ProjectVersion_AuthorId', 'ProjectVersion', 'authorId', 'User', 'id', $delete = 'CASCADE', $update = 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('ProjectVersion');
    }
}

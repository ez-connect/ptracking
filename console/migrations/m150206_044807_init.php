<?php

use yii\db\Schema;
use yii\db\Migration;

class m150206_044807_init extends Migration
{
    public function up()
    {
        // Table options
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('User', [
            'id'                    => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',
            'email'                 => 'VARCHAR(127) NOT NULL',
            'passwordHash'          => 'VARCHAR(127) NOT NULL',
            'passwordResetToken'    => 'VARCHAR(127)',
            'authKey'               => 'VARCHAR(127) NOT NULL',
            'activationToken'       => 'VARCHAR(127)',
            'name'                  => 'VARCHAR(127) NOT NULL',
            'role'                  => 'VARCHAR(127)',
            'avatar'                => 'VARCHAR(127)',
            'status'                => "ENUM('inactive', 'active', 'ban') DEFAULT 'active'",
            'loggedTime'            => 'DATETIME',
            'ip'                    => 'VARCHAR(127)',
            'createTime'            => 'DATETIME',
            'updateTime'            => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ], $tableOptions);

        // Index
        $this->createIndex('IDX_User_Email', 'User', 'email', true);
        $this->createIndex('IDX_User_Name', 'User', 'name', false);
        $this->createIndex('IDX_User_Status', 'User', 'status', false);

        // Record
        $this->insert('User', [
            'id'                    => 1,
            'email'                 => 'thanh.vinh@hotmail.com',
            'name'                  => 'Thanh Vinh',
            'passwordHash'          => Yii::$app->getSecurity()->generatePasswordHash('123456'),
            'authKey'               => Yii::$app->getSecurity()->generateRandomString(),
            'activationToken'       => Yii::$app->getSecurity()->generateRandomString(),
            'status'                => 'active',
            'createTime'            => date('Y-m-d H:i:s')
        ]);
    }

    public function down()
    {
        // User
        $this->dropTable('User');
    }
}

<?php

use yii\db\Schema;
use yii\db\Migration;

class m150210_040145_create_food_menu extends Migration
{
    public function up()
    {
        // Table options
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        //
        // FoodStore
        //
        $this->createTable('FoodStore', [
            'id'                    => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',
            'name'                  => 'VARCHAR(127) NOT NULL',
            'email'                 => 'VARCHAR(127)',
            'contact'               => 'VARCHAR(127)',
            'address'               => 'VARCHAR(127)',
            'tel'                   => 'VARCHAR(64)',
            'mobile'                => 'VARCHAR(64)',
            'status'                => "ENUM('inactive', 'active') DEFAULT 'active'",
            'authorId'              => 'INT UNSIGNED NOT NULL',
            'createTime'            => 'DATETIME',
            'updateTime'            => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ]);

        // Index
        $this->createIndex('IDX_FoodStore_Name', 'FoodStore', 'email', true);

        // Foreign key
        $this->addForeignKey('FK_FoodStore_AuthorId', 'FoodStore', 'authorId', 'User', 'id', $delete = 'CASCADE', $update = 'CASCADE');

        //
        // FoodMenu
        //
        $this->createTable('FoodMenu', [
            'id'                    => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',
            'foodStoreId'           => 'INT UNSIGNED NOT NULL',
            'name'                  => 'VARCHAR(64) NOT NULL',
            'price'                 => 'INTEGER UNSIGNED NOT NULL DEFAULT 0',
            'authorId'              => 'INT UNSIGNED NOT NULL',
            'createTime'            => 'DATETIME',
            'updateTime'            => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ], $tableOptions);

        // Index
        $this->createIndex('IDX_FoodMenu_Name', 'FoodMenu', 'name', true);

        // Foreign key
        $this->addForeignKey('FK_FoodMenu_FoodStoreId', 'FoodMenu', 'foodStoreId', 'FoodStore', 'id', $delete = 'CASCADE', $update = 'CASCADE');
        $this->addForeignKey('FK_FoodMenu_AuthorId', 'FoodMenu', 'authorId', 'User', 'id', $delete = 'CASCADE', $update = 'CASCADE');
    }

    public function down()
    {
       $this->dropTable('FoodMenu');
       $this->dropTable('FoodStore');
    }
}

-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2013 at 03:45 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ptracking`
--

-- --------------------------------------------------------

--
-- Dumping data for table `member`
-- admin / admin
--

INSERT INTO `Member` (`id`, `name`, `passwordHash`, `passwordResetToken`, `authKey`, `email`, `role`, `avatar`) VALUES
(1, 'admin', '$2y$13$iNdCCCdhpf/3jYjKYKq9vegmy9CC0x8sPatA0PRwQfQ4oEtNQ4hMG', NULL, 'MuPFUknbZQpLe6MWw4xD_t9JGZ77MJlw', 'thanh.vinh@hotmail.com', 60, NULL),
(2, 'Nguyen Thanh Vinh', '$2y$13$0p2puSWZJegeGjgkH3jMceSU.fo5d6uhfvmkMSFmZHHtCgnYjQRGu', NULL, 'PyzLlUFnBc.4HzvO-09WjgbQSDfGmQqu', 'vinh.nguyenthanh@gameloft.com', 60, 'xOnfLCgEdHDEdWkU7OfAi68bEb0YnKEn.png');

--
-- Dumping data for table `project`
--

INSERT INTO `Project` (`id`, `name`, `code`, `startDate`, `endDate`, `foodStoreId`, `authorId`) VALUES
(1, 'Amazing Spiderman 2', 'TASM', '2013-12-30', '2014-04-18', 1, 2),
(2, 'Project example #2', 'COD2', '2013-12-30', '2014-04-11', NULL, 2),
(3, 'Project example #3', 'COD3', '2013-12-30', '2014-04-11', NULL, 2);

--
-- Dumping data for table `screensize`
--

INSERT INTO `ScreenSize` (`id`, `name`) VALUES
(1, '2560x1600'),
(2, '1280x800'),
(3, '1204x600'),
(4, '800x480'),
(5, '640x480'),
(6, '640x360'),
(7, '480x360'),
(8, '480x320'),
(9, '400x240'),
(10, '320x240'),
(11, '220x176'),
(12, '160x128'),
(13, '128x128');

--
-- Dumping data for table `build`
--
INSERT INTO `Build` (`name`, `code`, `deviceTypeId`, `screenSizeId`, `priorityId`, `authorId`) VALUES
('Amazon Kindle Fire','Kindle Fire',1,2,1,2),
('Samsung GT-i9100 (Galaxy S II, Seine)','i9100',1,4,1,2),
('LG P500 (Optimus One, Thunder)','P500',1,8,1,2),
('Samsung GT-S8000 (Cubic 31, Jet)','S8000',2,4,1,2),
('Nokia 5800 XpressMusic (Tube, Space)','N5800',2,6,1,2),
('LG KU990 (Viewty, Nyx)','KU990',2,9,1,2),
('Samsung GT-S5230 (Tocco Lite, Ely, Star, Player One)','S5230',2,9,1,2),
('LG KP500 (KP501, KP505, S, Cookie)','KP500',2,9,1,2),
('LG GS290 (Janus, Cookie 2 fresh, Spice)','GS290',2,9,1,2),
('Sony-Ericsson Fengli (Fengy, Txt Pro, CK15A)','Fengli',2,9,1,2),
('Samsung SGH-F480 (Roxy,Player Style)','F480',2,9,1,2),
('Sony-Ericsson W150i (Yendo, Teacake)','W150i',2,10,1,2),
('BlackBerry 9930 Montana (R018)','BB9930',2,5,1,2),
('BlackBerry 9500 (9530, Thunder, Storm)','BB9500',2,7,1,2),
('Sony-Ericsson K800i (Wilma)','K800i',3,10,1,2),
('Nokia 6280','N6280',3,10,1,2),
('Nokia C2-05 (Boxter, Boulder, Cuba 2, RM-724)','C2-05',3,10,1,2),
('Nokia 6120 Classic (Wendy)','N6120',3,10,1,2),
('LG KS360 (Etna, TE365, GT360, GT365)','KS360',3,10,1,2),
('ZTE F160 (Reflect)','F160',3,10,1,2),
('Nokia C3-00 (Aerosmith, Siren, Douglas, Aggy, Baggy, Don, Diamond Rush, Haiti, RM-614)','C3-00',3,10,1,2),
('Nokia E71 (Hobbes/Piranha, Liam)','E71',3,10,1,2),
('Pantech P7040 (Slalom, Patriot, Link)','P7040',3,10,1,2),
('BlackBerry 8520 (Gemini, Dodger, Curve)','BB8520',3,10,1,2),
('BlackBerry 9700/9020 (Onyx, Bold 2)','BB9700',3,7,1,2),
('Samsung GT-C3510 (Genoa, Player Light, Corby POP)','C3510',2,10,1,2),
('Nokia X3-02 (X3-01, Leonardo, Lenox, Lasse, Lenny, Touch and Type, Lewis)','X3-02',2,10,1,2),
('LG T310 (Plum 2G, Wink Style, Cookie Style)','T310',2,10,1,2),
('Alcatel One Touch 602 (SFR 3440)','AOT 602',2,10,1,2),
('Motorola EX118 (Brea)','EX118',2,10,1,2),
('Motorola EX225 (Brea 3G)','EX225',2,10,1,2),
('ZTE X990 (SFR251, Rio, iChat Messenger)','X990',2,10,1,2),
('Nokia Asha 305 (Gaia DS, RM-766)','Asha 305',2,9,1,2),
('Huawei G7300 (T-Mobile Energy)','G7300',2,8,1,2),
('Nokia 5000d (5000)','N5000d',3,10,1,2),
('Samsung SGH-T479 (Gravity 3)','T479',3,10,1,2),
('Nokia X2-00 (Justin)','X2-00',3,10,1,2),
('LG GB230 (Julia)','GB230',3,11,1,2),
('Samsung GT-S3600','S3600',3,11,1,2),
('ZTE R100','R100',3,11,1,2),
('Nokia 5200 (Xpression)','N5200',3,12,1,2),
('Samsung SGH-E250','E250',3,12,1,2),
('Samsung GT-E2121B (Zinnia - I)','E2121B',3,12,1,2),
('Samsung GT-S3350 (Trevi, Ch@t 335)','S3350',3,10,1,2),
('Alcatel ONE TOUCH 800 (800A, Jade, Piano, Vairy Text, Chrome, Tribe)','AOT 800',3,10,1,2),
('LG C300 (Papy, Town)','C300',3,10,1,2),
('LG C365','C365',3,10,1,2),
('Samsung GT-C3222 (Punch, Ch@t 322)','C3222',3,11,1,2),
('Alcatel ONE TOUCH 585 (B11q)','AOT 585',3,11,1,2),
('Motorola EX116 (Silver-Q Mini)','EX116',3,11,1,2),
('Nokia 6030','N6030',3,13,1,2),
('Nokia 2760 (Pink)','N2760',3,12,1,2),
('Samsung GT-E2530 (Ivy)','E2530',3,12,1,1);

--
-- Dumping data for table `projectmember`
--

INSERT INTO `ProjectMember` (`id`, `projectId`, `memberId`, `authorId`) VALUES
(1, 1, 2, 2);

--
-- Dumping data for table `projectbuild`
--

INSERT INTO `ProjectBuild` (`id`, `projectId`, `buildId`, `authorId`) VALUES
(1, 1, 2, 2),
(2, 1, 3, 2),
(3, 1, 4, 2),
(4, 1, 16, 2),
(5, 1, 26, 2),
(6, 1, 15, 2);

--
-- Dumping data for table `projectassignment`
--

INSERT INTO `ProjectAssignment` (`id`, `projectId`, `projectBuildId`, `projectMemberId`, `authorId`) VALUES
(1, 1, 1, 1, 2);

--
-- Dumping data for table `task`
--
INSERT INTO `Task` (`id`, `projectId`, `projectBuildId`, `assigneeId`, `typeId`, `statusId`, `name`, `description`, `priorityId`, `startDate`, `endDate`, `authorId`, `modifierId`) VALUES
(1, 1, 3, 2, 1, 1, 'Jar-size below 2.8 MB', NULL, 2, DATE(NOW()), '2014-04-01', 2, 2),
(2, 1, 3, 2, 1, 1, 'FPS must be greater than 20', NULL, 2, DATE(NOW()), '2014-04-01', 2, 2);

INSERT INTO `FoodStore` (`id`, `name`, `contact`, `address`, `tel`, `mobile`, `authorId`) VALUES
(1, 'Cơm Huế', 'Ms Hằng', '22 Thái Phiên, Đà Nẵng', '', '0905613363', 2);

INSERT INTO `FoodMenu` (`id`, `foodStoreId`, `name`, `authorId`) VALUES
(1, 1, 'Cơm chay', 2),
(2, 1, 'Gà kho sả', 2),
(3, 1, 'Gà rô ti', 2),
(4, 1, 'Sườn non rim', 2),
(5, 1, 'Sườn non chua ngọt', 2),
(6, 1, 'Sườn cốt lếch ram', 2),
(7, 1, 'Thịt kho trứng', 2),
(8, 1, 'Thịt luộc mắm tôm', 2),
(9, 1, 'Tôm kho thịt', 2),
(10, 1, 'Đậu khuôn sốt cà', 2),
(11, 1, 'Trứng chiên/luộc', 2),
(12, 1, 'Cá kho tộ', 2),
(13, 1, 'Cá ngừ kho nước', 2),
(14, 1, 'Cá sốt cà', 2),
(15, 1, 'Cá chiên', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

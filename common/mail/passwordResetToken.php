<?php
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\User $user
 */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->passwordResetToken]);
?>

Hello <?= Html::encode($user->name) ?>,

follow the link below to reset your password:

<?= Html::a(Html::encode($resetLink), $resetLink) ?>

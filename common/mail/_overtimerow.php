<?php

use ez\helpers\DateTime;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Overtime $model
 */

$task = $model->task;
?>

<tr>
	<td align="right"><?= $index + 1 ?></td>
	<td align="left"><?= Html::a($task->name, '#') ?></td>
	<td align="left"><?= $task->assignee->name ?></td>
	<td align="left"><?= $task->projectBuild->build->code ?></td>
	<td align="left"><?= DateTime::niceShort($model->workDate) ?></td>
	<td align="left"><?= $model->startTime ?></td>
	<td align="left"><?= $model->endTime ?></td>
	<td align="left"><?= $model->menu->name ?></td>
</tr>

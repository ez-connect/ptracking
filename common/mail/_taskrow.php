<?php

use ez\helpers\DateTime;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Task $model
 */
?>

<tr>
	<td align="right"><?= $index + 1 ?></td>
	<td align="left"><?= Html::a($model->name, '#') ?></td>
	<td align="left"><?= $model->assignee->name ?></td>
	<td align="left"><?= $model->priority ?></td>
	<td align="left"><?= $model->projectBuild->build->code ?></td>
	<td align="left"><?= DateTime::niceShort($model->endDate) ?></td>
	<td align="left"><?= $model->status ?></td>
</tr>

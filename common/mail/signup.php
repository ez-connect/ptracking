<?php
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\User $user;
 */
?>

Hello <?= Html::encode($user->name) ?>,

<br />

Thank for your registration. Have a nice day!

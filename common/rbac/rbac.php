<?php

use common\models\Role;
use yii\rbac\Item;

return [
	// HERE ARE YOUR MANAGEMENT TASKS
	// Role::ROLE_OPERATION_UPDATE => ['type' => Item::TYPE_OPERATION, 'description' => '...', 'bizRule' => NULL, 'data' => NULL],
	// Role::ROLE_OPERATION_DELETE => ['type' => Item::TYPE_OPERATION, 'description' => '...', 'bizRule' => NULL, 'data' => NULL],

	// AND THE ROLES
	Role::ROLE_MEMBER => [
		'type' => Item::TYPE_ROLE,
		'description' => 'Member',
		'children' => [
		],
		'bizRule' => 'return !Yii::$app->user->isGuest;',
		'data' => NULL
	],

	Role::ROLE_SUPEVISOR => [
		'type' => Item::TYPE_ROLE,
		'description' => 'Supervisor',
		'children' => [
			Role::ROLE_MEMBER					// Can manage all that user can
		],
		'bizRule' => NULL,
		'data' => NULL
	],

	Role::ROLE_MANAGER => [
		'type' => Item::TYPE_ROLE,
		'description' => 'Manager',
		'children' => [
			Role::ROLE_MEMBER					// can do all the stuff that supervisor can
		],
		'bizRule' => NULL,
		'data' => NULL
	],

	Role::ROLE_ADMINISTRATOR => [
		'type' => Item::TYPE_ROLE,
		'description' => 'Administrator',
		'children' => [
			Role::ROLE_MANAGER					// can do everything
		],
		'bizRule' => NULL,
		'data' => NULL
	],
];

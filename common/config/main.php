<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    // 'timeZone' => 'Asia/Saigon', //UTC +7
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            // 'class' => 'common\rbac\PhpManager',
        ],
        // 'urlManager' => [
        //  here is your normal backend url manager config
        //],
        //'urlManagerFrontend' => [
        //  here is your frontend URL manager config
        //],
    ],
];

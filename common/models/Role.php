<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Role, no table.
 */
class Role
{
//	const ROLE_GUEST					= 0;
	const ROLE_MEMBER	 				= 10;
	// const ROLE_UPDATE	 				= 20;	// action, dynamic
	// const ROLE_DELETE	 				= 30;	// action, dynamic
	const ROLE_SUPEVISOR				= 40;
	const ROLE_MANAGER					= 50;
	const ROLE_ADMINISTRATOR			= 60;
	
	private static $ROLE = [
		self::ROLE_MEMBER 				=> 'Member',
		self::ROLE_SUPEVISOR 			=> 'Supevisor',
		self::ROLE_MANAGER 				=> 'Manager',
		self::ROLE_ADMINISTRATOR		=> 'Administrator',
	];
	
	public static function getName($id)
	{
		return static::$ROLE[$id];
	}
	
	public static function getArray()
	{
		$roles = static::$ROLE;
		if (Yii::$app->user->identity->role != self::ROLE_ADMINISTRATOR) {
			ArrayHelper::remove($roles, self::ROLE_ADMINISTRATOR);
		}
		return $roles;
	}
}

<?php

namespace common\models;

use Yii;
use yii\helpers\BaseFileHelper;
use yii\helpers\Security;

/**
 * This is the model class for table "TaskAttachment".
 *
 * @property integer $id
 * @property integer $taskId
 * @property string $attachment
 * @property string $baseName
 *
 * @property Task $task
 */
class TaskAttachment extends \yii\db\ActiveRecord
{
	const MAX_FILE_SIZE = 10485760;	//10 * 1024 * 1024 ~ 10 MB
	const PATH_ATTACHMENT = 'upload/attachments';
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'TaskAttachment';
	}
	
	public static function getRealPath()
	{
		return Yii::getAlias('@app') . '/web/' . self::PATH_ATTACHMENT;
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['taskId', 'attachment'], 'required'],
			[['taskId'], 'integer'],
			['baseName', 'file', 'maxSize' => self::MAX_FILE_SIZE /* , 'types' => 'png, jpg, jpeg, gif, zip', 'on' => 'insert' */],
			[['attachment'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'taskId' => 'Task',
			'attachment' => 'Attachment',
			'baseName' => 'Attachment',
		];
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getTask()
	{
		return $this->hasOne(Task::className(), ['id' => 'taskId']);
	}
	
	public function getAttachmentPath()
	{
		$filename = self::PATH_ATTACHMENT . "/$this->attachment";
		return $filename;
	}
		
	public function generateValues($task, $uploadFile)
	{
		$year = date('Y');
		$month = date('m');
		$path = static::getRealPath() . "/$year/$month";
		BaseFileHelper::createDirectory($path);
		
		$filename = Security::generateRandomKey() . '.' . $uploadFile->getExtension();
		$uploadFile->saveAs($path . '/' . $filename);
		
		$this->taskId = $task->id;
		$this->baseName = $uploadFile->name;
		$this->url = "$year/$month/$filename";
	}
}

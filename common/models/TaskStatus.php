<?php

namespace common\models;

use yii\helpers\Html;

/**
 * The status of tasks, no store table.
 */
class TaskStatus
{
	const STATUS_NEW 					= 1;
	const STATUS_ON_HOLD 				= 2;
	const STATUS_RESOLVED				= 3;
	const STATUS_OPEN					= 4;
	const STATUS_DUPLICATE				= 5;
	const STATUS_INVALID				= 6;
	const STATUS_WONT_FIX				= 7;
	const STATUS_SPAM					= 8;
	
	const STATUS_UNRESOLVED				= 10;	//STATUS_NEW | STATUS_ON_HOLD | STATUS_OPEN
	const STATUS_CLOSED					= 20;	//STATUS_RESOLVED | STATUS_DUPLICATE | STATUS_INVALID | STATUS_WONT_FIX | STATUS_SPAM
	const STATUS_ALL					= 0;	//STATUS_UNRESOLVED | STATUS_RESOLVED | STATUS_DUPLICATE | STATUS_INVALID | STATUS_WONT_FIX | STATUS_SPAM
	
	private static $STATUS = [
		self::STATUS_NEW 				=> 'new',
		self::STATUS_ON_HOLD 			=> 'on hold',
		self::STATUS_RESOLVED 			=> 'resolved',
		self::STATUS_OPEN 				=> 'open',
		self::STATUS_DUPLICATE 			=> 'duplicate',
		self::STATUS_INVALID 			=> 'invalid',
		self::STATUS_WONT_FIX 			=> 'wont fix',
		self::STATUS_SPAM 				=> 'spam',
		self::STATUS_UNRESOLVED			=> 'unresolved',
		self::STATUS_CLOSED				=> 'closed',
	];
	
	private static $FORMAT = [
		self::STATUS_NEW 				=> 'primary',
		self::STATUS_ON_HOLD 			=> 'default',
		self::STATUS_RESOLVED 			=> 'success',
		self::STATUS_OPEN 				=> 'danger',
		self::STATUS_DUPLICATE 			=> 'info',
		self::STATUS_INVALID 			=> 'info',
		self::STATUS_WONT_FIX 			=> 'info',
		self::STATUS_SPAM 				=> 'warning',
	];
	
	public static function getArray()
	{
		return static::$STATUS;
	}
	
	public static function getName($id)
	{
		return static::$STATUS[$id];
	}
	
	public static function getLabel($id)
	{
		return Html::tag('span', static::getName($id), [
			'class' => 'label label-' . static::$FORMAT[$id]
		]);
	}
	
	public static function getWorkflow($task)
	{
		$flows = [];
		if ($task->statusId == self::STATUS_RESOLVED) {
			$flows = [
				self::STATUS_ON_HOLD,
				self::STATUS_OPEN,
				self::STATUS_DUPLICATE,
				self::STATUS_INVALID,
				self::STATUS_WONT_FIX,
				self::STATUS_SPAM,
			];
		} else {
			$flows = [
				self::STATUS_ON_HOLD,
				self::STATUS_RESOLVED,
				self::STATUS_DUPLICATE,
				self::STATUS_INVALID,
				self::STATUS_WONT_FIX,
				self::STATUS_SPAM,
			];
		}
		
		return $flows;
	}
}

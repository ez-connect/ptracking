<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Build".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property integer $deviceTypeId
 * @property integer $screenSizeId
 * @property string $authorId
 * @property string $createTime
 * @property string $updateTime
 *
 * @property Member $author
 * @property Feedback[] $feedbacks
 */
class Build extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'Build';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name', 'authorId'], 'required'],
			[['authorId'], 'integer'],
			[['createTime', 'updateTime'], 'safe'],
			[['name'], 'string', 'max' => 255],
			[['code'], 'string', 'max' => 32],
			[['name', 'code'], 'unique'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Name',
			'authorId' => 'Added by',
			'createTime' => 'Create',
			'updateTime' => 'Update',
		];
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getAuthor()
	{
		return $this->hasOne(Member::className(), ['id' => 'authorId']);
	}
	
	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getFeedbacks()
	{
		return $this->hasMany(Feedback::className(), ['buildId' => 'id']);
	}
		
	/**
	 * Get the builds which don't show.
	 */
	public static function getNotInId($projectId)
	{
		$builds = ProjectBuild::find()
			->select('buildId')
			->where(['projectId' => $projectId])
			->column();
		
		return $builds;
	}
	
	/**
	 * Get the build list.
	 */
	public static function getArrayByProjectId($projectId)
	{
		$addedBuilds = static::getNotInId($projectId);
		$builds = Build::find()
			->select(['id', 'name'])
			->where(['not in', 'id', $addedBuilds])
			->all();

		$names = ArrayHelper::map($builds, 'id', 'name');
		$names = ArrayHelper::htmlEncode($names);
			
		return $names;
	}
}

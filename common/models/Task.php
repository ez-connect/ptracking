<?php

namespace common\models;

use yii\data\ActiveDataProvider;
use yii\web\ErrorException;

/**
 * This is the model class for table "Task".
 *
 * @property integer $id
 * @property integer $projectId
 * @property integer $projectBuildId
 * @property integer $assigneeId
 * @property integer $typeId
 * @property integer $statusId
 * @property string $name
 * @property string $description
 * @property integer $priorityId
 * @property string $startDate
 * @property string $endDate
 * @property integer $authorId
 * @property integer $modifierId
 * @property string $createTime
 * @property string $updateTime
 *
 * @property string $priority
 * @property Taskstatus $status
 * @property Project $project
 * @property ProjectBuild $projectBuild
 * @property User $assignee
 * @property string $type
 * @property string $status
 * @property TaskAttachment[] $attachments
 * @property TaskComment[] $comments
 * @property User $author
 * @property User $modifier
 * @property boolean $isOvertime write-only attribute
 */
class Task extends \yii\db\ActiveRecord
{
	public $isOvertime = false;
	
	/**
	 * @inheritdoc
	 */
	public function __construct($config = [])
	{
		parent::__construct($config);
		$today = date('Y-m-d');
		$this->startDate = $today;
		$this->endDate = $today;
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'Task';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['projectBuildId', 'assigneeId', 'typeId', 'name', 'startDate', 'endDate', 'authorId', 'modifierId'], 'required'],
			[['projectBuildId', 'assigneeId', 'typeId', 'priorityId', 'statusId', 'authorId', 'modifierId'], 'integer'],
			[['startDate', 'endDate', 'createTime', 'updateTime'], 'safe'],
			[['name'], 'string', 'max' => 255],
			[['description'], 'string'],
			[['isOvertime'], 'boolean'],
			
			['endDate', 'compare', 'compareAttribute' => 'startDate', 'operator' => '>=',
				'message'=>'{attribute} must be greater than or equal {compareValue}.'
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'projectBuildId' => 'Build',
			'assigneeId' => 'Assignee',
			'typeId' => 'Type',
			'statusId' => 'Status',
			'name' => 'Name',
			'description' => 'Description',
			'startDate' => 'Start',
			'endDate' => 'End',
			'priorityId' => 'Priority',
			'authorId' => 'Author',
			'modifierId' => 'By',
			'createTime' => 'Created',
			'updateTime' => 'Updated',
			'isOvertime' => 'Overtime',
		];
	}
	
	/**
	 * Fill Project.authorId before call validate.
	 * @inheritdoc
	 */
	public function beforeValidate()
	{
		if ($this->isNewRecord) {
			$this->authorId = $this->modifierId = \Yii::$app->user->getId();
			$this->statusId = TaskStatus::STATUS_NEW;
		}
		return parent::beforeValidate();
	}
	
	/**
	 * return string
	 */
	public function getPriority()
	{
		return Priority::getName($this->priorityId);
	}
	
	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getProject()
	{
		return $this->hasOne(Project::className(), ['id' => 'projectId']);
	}
	
	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getProjectBuild()
	{
		return $this->hasOne(ProjectBuild::className(), ['id' => 'projectBuildId']);
	}
	
	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getAssignee()
	{
		return $this->hasOne(User::className(), ['id' => 'assigneeId']);
	}
	
	/**
	 * @return string
	 */
	public function getType()
	{
		return TaskType::getName($this->typeId);
	}
	
	/**
	 * @return string	html span
	 */
	public function getStatus()
	{
		return TaskStatus::getName($this->statusId);
	}
	
	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getAttachments()
	{
		return $this->hasMany(TaskAttachment::className(), ['taskId' => 'id']);
	}
	
	/**
	 * @return \yii\data\ActiveDataProvider
	 */
	public function getComments()
	{
		$query = TaskComment::find()
			->where(['taskId' => $this->id])
			->orderBy('id DESC');
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => false,
		]);
		
		return $dataProvider;
	}
	
	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getAuthor()
	{
		return $this->hasOne(User::className(), ['id' => 'authorId']);
	}
	
	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getModifier()
	{
		return $this->hasOne(User::className(), ['id' => 'modifierId']);
	}
	
	/**
	 * @return \common\models\Task[]
	 */
	public function getAssignedTasks($userId, $filter)
	{
		$query = static::find()
		->innerJoin('ProjectAssignment', 'ProjectAssignment.id = Task.projectBuildId')
		->innerJoin('ProjectMember', 'ProjectMember.id = ProjectAssignment.projectMemberId')
		->innerJoin('Member', 'Member.id = ProjectMember.memberId AND Member.id = :id', [':id' => $userId]);
		
		$models = $query->all();
		return $models;
	}
	
	public static function getSummary($projectId, $typeId, $endDate)
	{
		$summary = [
			'date' => $endDate,
			'total' => static::find()
				->select('id')
				->where(['projectId' => $projectId, 'typeId' => $typeId, 'endDate' => $endDate])
				->count(),
			'closed' => static::find()
				->select('id')
				->where(['projectId' => $projectId, 'typeId' => $typeId, 'endDate' => $endDate])
				->andWhere(['statusId' => [
					TaskStatus::STATUS_RESOLVED, 
					TaskStatus::STATUS_DUPLICATE,
					TaskStatus::STATUS_INVALID,
					TaskStatus::STATUS_WONT_FIX,
					TaskStatus::STATUS_SPAM,
				]])
				->count(),
		];
		
		// \yii\helpers\VarDumper::dump($summary, 10, true); exit();
		return $summary;
	}
}

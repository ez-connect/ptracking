<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "FoodStore".
 *
 * @property integer $id
 * @property string $name
 * @property string $contact
 * @property string $address
 * @property string $tel
 * @property string $mobile
 * @property string $authorId
 * @property string $createTime
 * @property string $updateTime
 *
 * @property Foodmenu[] $foodmenus
 * @property Member $author
 */
class FoodStore extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'FoodStore';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name', 'authorId'], 'required'],
			[['authorId'], 'integer'],
			[['createTime', 'updateTime'], 'safe'],
			[['name', 'contact', 'address', 'tel', 'mobile'], 'string', 'max' => 64]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Name',
			'contact' => 'Contact',
			'address' => 'Address',
			'tel' => 'Tel. number',
			'mobile' => 'Mobile number',
			'authorId' => 'By',
			'createTime' => 'Create',
			'updateTime' => 'Update',
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function beforeValidate()
	{
		$this->authorId = \Yii::$app->user->getId();
		return parent::beforeValidate();
	}
	

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getFoodmenus()
	{
		return $this->hasMany(Foodmenu::className(), ['foodStoreId' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getAuthor()
	{
		return $this->hasOne(User::className(), ['id' => 'authorId']);
	}
	
	public static function getArray() {
		$stores = static::find()->all();
		$stores = ArrayHelper::map($stores, 'id', 'name');
		
		return $stores;
	}
}

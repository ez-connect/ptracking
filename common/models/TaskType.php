<?php

namespace common\models;

/**
 * Task types, instead of "TaskType" table.
 */
class TaskType
{
	const TYPE_TARGET 					= 1;
	const TYPE_ISSUE 					= 2;
	const TYPE_FEEDBACK					= 3;
	const TYPE_REQUEST					= 4;
	const TYPE_MILESTONE				= 5;
	const TYPE_MICRO_PLANNING			= 6;
	
	private static $TYPES = [
		self::TYPE_TARGET => 'target',
		self::TYPE_ISSUE => 'issue',
		self::TYPE_FEEDBACK => 'feedback',
		self::TYPE_REQUEST => 'request',
		self::TYPE_MILESTONE => 'milestone',
		self::TYPE_MICRO_PLANNING => 'micro planning',
	];
	
	public static function getName($id)
	{
		return static::$TYPES[$id];
	}
	
	public static function getArray()
	{
		return static::$TYPES;
	}
}

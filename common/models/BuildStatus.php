<?php

namespace common\models;

use yii\helpers\Html;

/**
 * The status of a build.
 *
 */
class BuildStatus
{
	const STATUS_NA								= 0x0;
	const STATUS_NOT_YET						= 0x1;
	const STATUS_FAILED							= 0x2;
	const STATUS_IN_TEST						= 0x4;
	const STATUS_PASSED							= 0x8;
	const STATUS_PASS_WARNING					= 0x10;
	
	private static $STATUS = [
		self::STATUS_NOT_YET => 'NY',
		self::STATUS_FAILED => 'F',
		self::STATUS_IN_TEST => 'I',
		self::STATUS_PASSED => 'P',
		self::STATUS_PASS_WARNING => 'WN',
		self::STATUS_NA => 'NA',
	];
	
	private static $STATUS_FULL = [
		self::STATUS_NOT_YET => 'not yet',
		self::STATUS_FAILED => 'failed',
		self::STATUS_IN_TEST => 'in-test',
		self::STATUS_PASSED => 'passed',
		self::STATUS_PASS_WARNING => 'passed w/n',
		self::STATUS_NA => 'n/a',
	];
	
	private static $FORMAT = [
		self::STATUS_NOT_YET => 'default',
		self::STATUS_FAILED => 'danger',
		self::STATUS_IN_TEST => 'primary',
		self::STATUS_PASSED => 'success',
		self::STATUS_PASS_WARNING => 'warning',
		self::STATUS_NA => 'success',
	];
	
	public static function getArray()
	{
		return static::$STATUS_FULL;
	}
	
	public static function getName($id, $isFullName = false)
	{
		if (!$isFullName) {
			return static::$STATUS[$id];
		} else {
			return static::$STATUS_FULL[$id];
		}
	}
	
	public static function getText($id, $isFullName = false)
	{
		return Html::tag('span', static::getName($id, $isFullName), [
			'class' => 'text-' . static::$FORMAT[$id]
		]);
	}
	
	public static function getLabel($id)
	{
		return Html::tag('span', static::getName($id), [
			'class' => 'label label-' . static::$FORMAT[$id]
		]);
	}
	
	public static function getTextLabel($id)
	{
		return (static::getText($id, true) . '&nbsp;'	. static::getLabel($id));
	}
}

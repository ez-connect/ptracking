<?php

namespace common\models;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ProjectMember".
 *
 * @property integer $id
 * @property integer $projectId
 * @property integer $memberId
 * @property integer $authorId
 * @property string $createTime
 *
 * @property Member $author
 * @property Member $member
 * @property Project $project
 */
class ProjectMember extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'ProjectMember';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['projectId', 'memberId', 'authorId'], 'required'],
			[['projectId', 'memberId', 'authorId'], 'integer'],
			[['createTime'], 'safe']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'projectId' => 'Project',
			'memberId' => 'Member',
			'authorId' => 'Added by',
			'createTime' => 'Create',
		];
	}

	/**
	 * Fill Project.authorId before call validate.
	 * @inheritdoc
	 */
	public function beforeValidate()
	{
		if ($this->isNewRecord) {
			$this->authorId = Yii::$app->user->getId();
		}
		return parent::beforeValidate();
	}
	
	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getAuthor()
	{
		return $this->hasOne(User::className(), ['id' => 'authorId']);
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getMember()
	{
		return $this->hasOne(User::className(), ['id' => 'memberId']);
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getProject()
	{
		return $this->hasOne(Project::className(), ['id' => 'projectId']);
	}
	
	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getAssignments()
	{
		return $this->hasMany(ProjectAssignment::className(), ['projectMemberId' => 'id']);
	}
	
	/**
	 * Check the record was exists.
	 * @return boolean
	 */
	public static function isExist($model)
	{
		$projectMember = static::find()
			->where(['memberId' => $model->memberId])
			->andWhere(['projectId' => $model->projectId])
			->one();
		$isExist = ($projectMember != null);
			
		return $isExist;
	}
	
	/**
	 * Get the user lists.
	 * @param integer $projectId
	 * @param bool $isMemberId		get Member.id or ProjectMember.id, default is Member.id
	 * @TODO should base on the roles.
	 */
	public static function getArrayByProjectId($projectId, $isMemberId = true)
	{
		$members = static::find()
			->with('member')
			->where(['projectId' => $projectId])
			->all();
		
		// \yii\helpers\VarDumper::dump($members, 10, true); exit();
		if ($isMemberId) {
			$members = ArrayHelper::map($members, 'member.id', 'member.name');
		} else {
			$members = ArrayHelper::map($members, 'id', 'member.name');
		}
		return $members;
	}
}

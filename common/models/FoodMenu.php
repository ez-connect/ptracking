<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "FoodMenu".
 *
 * @property string $id
 * @property integer $foodStoreId
 * @property string $name
 * @property integer $price
 * @property string $authorId
 * @property string $createTime
 * @property string $updateTime
 *
 * @property Member $author
 * @property Foodstore $foodStore
 * @property Overtime[] $overtimes
 */
class FoodMenu extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'FoodMenu';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['foodStoreId', 'name', 'authorId',], 'required'],
			[['foodStoreId', 'price', 'authorId'], 'integer'],
			[['createTime', 'updateTime'], 'safe'],
			[['name'], 'string', 'max' => 64]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'foodStoreId' => 'Food Store',
			'name' => 'Name',
			'price' => 'Price',
			'authorId' => 'By',
			'createTime' => 'Create',
			'updateTime' => 'Update',
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function beforeValidate()
	{
		$this->authorId = \Yii::$app->user->getId();
		return parent::beforeValidate();
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getAuthor()
	{
		return $this->hasOne(User::className(), ['id' => 'authorId']);
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getFoodStore()
	{
		return $this->hasOne(Foodstore::className(), ['id' => 'foodStoreId']);
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getOvertimes()
	{
		return $this->hasMany(Overtime::className(), ['menuId' => 'id']);
	}
	
	public static function getArray($foodStoreId) {
		$menus = static::find()
			->where(['foodStoreId' => $foodStoreId])
			->all();
		$menus = ArrayHelper::map($menus, 'id', 'name');
		
		// \yii\helpers\VarDumper::dump($menus, 10, true);exit();
		return $menus;
	}
}

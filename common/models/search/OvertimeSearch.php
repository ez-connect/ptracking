<?php

namespace common\models\search;

use common\models\Overtime;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * FoodStoreSearch represents the model behind the search form about FoodStore.
 */
class OvertimeSearch extends Model
{
	public $id;
	public $projectId;
	public $taskId;
	public $workDate;
	public $startTime;
	public $endTime;
	public $menuId;

	public function rules()
	{
		return [
			[['projectId'], 'required'],
			[['projectId', 'taskId', 'menuId'], 'integer'],
			[['workDate', 'startTime', 'endTime'], 'safe'],
		];
	}
	
	public function search($params)
	{
		$query = Overtime::find();
		
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['id' => SORT_DESC],
			]
		]);

		if (!($this->load($params, '') && $this->validate())) {
			return $dataProvider;
		}

		$this->addCondition($query, 'id');
		$this->addCondition($query, 'projectId');
		$this->addCondition($query, 'taskId');
		$this->addCondition($query, 'workDate');
		$this->addCondition($query, 'startTime');
		$this->addCondition($query, 'endTime');
		$this->addCondition($query, 'menuId');
		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}

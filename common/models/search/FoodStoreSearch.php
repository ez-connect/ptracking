<?php

namespace common\models\search;

use common\models\FoodStore;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * FoodStoreSearch represents the model behind the search form about FoodStore.
 */
class FoodStoreSearch extends Model
{
	public $id;
	public $name;
	public $author;
	public $createTime;
	public $updateTime;

	public function rules()
	{
		return [
			[['id', 'author'], 'integer'],
			[['name', 'createTime', 'updateTime'], 'safe'],
		];
	}
	
	public function search($params)
	{
		$query = FoodStore::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['updateTime' => SORT_DESC],
			]
		]);

		if (!($this->load($params, '') && $this->validate())) {
			return $dataProvider;
		}

		$this->addCondition($query, 'id');
		$this->addCondition($query, 'name', true);
		$this->addCondition($query, 'author');
		$this->addCondition($query, 'createTime');
		$this->addCondition($query, 'updateTime');
		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}

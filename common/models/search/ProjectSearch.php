<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Project;

/**
 * ProjectSearch represents the model behind the search form about Project.
 */
class ProjectSearch extends Model
{
	public $id;
	public $name;
	public $code;
	public $startDate;
	public $endDate;
	public $authorId;
	public $createTime;
	public $updateTime;

	public function rules()
	{
		return [
			[['id', 'authorId'], 'integer'],
			[['name', 'startDate', 'endDate', 'createTime', 'updateTime'], 'safe'],
		];
	}
	
	public function search($params)
	{
		$query = Project::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['endDate' => SORT_DESC],
			]
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$this->addCondition($query, 'name');
		$this->addCondition($query, 'name', true);
		return $dataProvider;
	}
	
	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}

<?php

namespace common\models\search;

use common\models\Task;
use common\models\TaskStatus;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TaskSearch represents the model behind the search form about Task.
 */
class TaskSearch extends Model
{
	const TASK_ALL 				= 0;
	const TASK_TODAY 			= 1;
	const TASK_YESTERDAY 		= 2;
	const TASK_THISWEEK 		= 3;
	
	public $id;
	public $projectId;
	public $projectBuildId;
	public $assigneeId;
	public $typeId;
	public $statusId;
	public $name;
	public $description;
	public $priorityId;
	public $startDate;
	public $endDate;
	public $authorId;
	public $modifierId;
	public $createTime;
	public $updateTime;
	
	public $filter;
	public $status;

	public function rules()
	{
		return [
			[['id', 'projectId', 'projectBuildId', 'assigneeId', 'typeId', 'statusId', 'priorityId', 'authorId', 'modifierId', 'filter', 'status'], 'integer'],
			[['name', 'description', 'startDate', 'endDate', 'createTime', 'updateTime'], 'safe'],
		];
	}
	
	public function search($params)
	{
		$query = Task::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['updateTime' => SORT_DESC],
			]
		]);

		if (!($this->load($params, '') && $this->validate())) {
			return $dataProvider;
		}

		$this->addCondition($query, 'id');
		$this->addCondition($query, 'projectId');
		$this->addCondition($query, 'projectBuildId');
		$this->addCondition($query, 'assigneeId');
		$this->addCondition($query, 'typeId');
		$this->addCondition($query, 'statusId');
		$this->addCondition($query, 'name', true);
		$this->addCondition($query, 'description', true);
		$this->addCondition($query, 'priorityId');
		$this->addCondition($query, 'startDate');
		$this->addCondition($query, 'endDate');
		$this->addCondition($query, 'authorId');
		$this->addCondition($query, 'modifierId');
		$this->addCondition($query, 'createTime');
		$this->addCondition($query, 'updateTime');
		// Custom
		$this->addCustomCondition($query, 'filter');
		$this->addCustomCondition($query, 'status');
		
		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
	
	protected function addCustomCondition($query, $attribute)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		
		if ($attribute == 'filter') {
			if ($value == self::TASK_TODAY) {
				$query->andWhere('DAYOFYEAR(endDate) = DAYOFYEAR(NOW())');
			} elseif ($value == self::TASK_YESTERDAY) {
				$query->andWhere('DAYOFYEAR(endDate) = (DAYOFYEAR(NOW()) - 1)');
			} elseif ($value == self::TASK_THISWEEK) {
				$query->andWhere('WEEKOFYEAR(endDate) = WEEKOFYEAR(NOW())');
			}
		}
		
		if ($attribute == 'status') {
			if ($value == TaskStatus::STATUS_UNRESOLVED) {
				$query->andWhere(['statusId' => [
					TaskStatus::STATUS_NEW,
					TaskStatus::STATUS_ON_HOLD,
					TaskStatus::STATUS_OPEN
				]]);
			} elseif ($value == TaskStatus::STATUS_CLOSED) {
				$query->andWhere(['statusId' => [
					TaskStatus::STATUS_RESOLVED, 
					TaskStatus::STATUS_DUPLICATE,
					TaskStatus::STATUS_INVALID,
					TaskStatus::STATUS_WONT_FIX,
					TaskStatus::STATUS_SPAM,
				]]);
			}
		}
	}
	
	public static function getFilters()
	{
		$filters = [
			self::TASK_ALL,
			self::TASK_TODAY,
			self::TASK_YESTERDAY,
			self::TASK_THISWEEK,
		];
		return $filters;
	}
	
	public static function getFilterName($filter)
	{
		switch ($filter) {
			case self::TASK_TODAY:
				return 'today';
			case self::TASK_YESTERDAY:
				return 'yesterday';
			case self::TASK_THISWEEK:
				return 'this week';
			default:
				return 'all';
		}
	}
}

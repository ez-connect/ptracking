<?php

namespace common\models\search;

use common\models\ProjectAssignment;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ProjectAssignmentSearch represents the model behind the search form about ProjectAssignment.
 */
class ProjectAssignmentSearch extends Model
{
	public $id;
	public $projectId;
	public $projectBuildId;
	public $projectMemberId;
	public $authorId;
	public $createTime;
	public $modifyTime;

	public function rules()
	{
		return [
			[['id', 'projectId', 'projectBuildId', 'projectMemberId', 'authorId'], 'integer'],
			[['createTime', 'modifyTime'], 'safe'],
		];
	}
	
	public function search($params)
	{
		$query = ProjectAssignment::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['createTime' => SORT_DESC],
			]
		]);

		if (!($this->load($params, '') && $this->validate())) {
			return $dataProvider;
		}

		$this->addCondition($query, 'id');
		$this->addCondition($query, 'projectId');
		$this->addCondition($query, 'projectBuildId');
		$this->addCondition($query, 'projectMemberId');
		$this->addCondition($query, 'authorId');
		$this->addCondition($query, 'createTime');
		$this->addCondition($query, 'modifyTime');
		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}

<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ProjectMember;

/**
 * ProjectMemberSearch represents the model behind the search form about ProjectMember.
 */
class ProjectMemberSearch extends Model
{
	public $id;
	public $projectId;
	public $memberId;
	public $authorId;
	public $createTime;

	public function rules()
	{
		return [
			[['id', 'projectId', 'memberId', 'authorId'], 'integer'],
			[['createTime'], 'safe'],
		];
	}
	
	public function search($params)
	{
		$query = ProjectMember::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['createTime' => SORT_DESC],
			]
		]);

		if (!($this->load($params, '') && $this->validate())) {
			return $dataProvider;
		}

		$this->addCondition($query, 'id');
		$this->addCondition($query, 'projectId');
		$this->addCondition($query, 'memberId');
		$this->addCondition($query, 'authorId');
		$this->addCondition($query, 'createTime');
		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}

<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MemberAssigment;

/**
 * MemberAssigmentSearch represents the model behind the search form about MemberAssigment.
 */
class MemberAssigmentSearch extends Model
{
	public $id;
	public $projectId;
	public $buildId;
	public $memberId;
	public $authorId;
	public $createTime;

	public function rules()
	{
		return [
			[['id', 'projectId', 'buildId', 'memberId', 'authorId'], 'integer'],
			[['createTime'], 'safe'],
		];
	}
	
	public function search($params)
	{
		$query = MemberAssigment::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['createTime' => SORT_DESC],
			]
		]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}

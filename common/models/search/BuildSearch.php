<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Build;

/**
 * BuildSearch represents the model behind the search form about Build.
 */
class BuildSearch extends Model
{
	public $id;
	public $name;
	public $deviceTypeId;
	public $screenSizeId;
	public $priorityId;
	public $authorId;
	public $createTime;
	public $updateTime;

	public function rules()
	{
		return [
			[['id', 'deviceTypeId', 'screenSizeId', 'priorityId', 'authorId'], 'integer'],
			[['name', 'createTime', 'updateTime'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Name',
			'deviceTypeId' => 'Device Type ID',
			'screenSizeId' => 'Screen Size ID',
			'priorityId' => 'Priority ID',
			'authorId' => 'Author ID',
			'createTime' => 'Create Time',
			'updateTime' => 'Update Time',
		];
	}

	public function search($params)
	{
		$query = Build::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['endDate' => SORT_DESC],
			]
		]);

		if (!($this->load($params, '') && $this->validate())) {
			return $dataProvider;
		}

		$this->addCondition($query, 'name');
		$this->addCondition($query, 'name', true);
		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}

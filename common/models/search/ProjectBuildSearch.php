<?php

namespace common\models\search;

use common\models\ProjectAssignment;
use common\models\ProjectBuild;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ProjectBuildSearch represents the model behind the search form about ProjectBuild.
 */
class ProjectBuildSearch extends Model
{
	public $id;
	public $projectId;
	public $buildId;
	public $feedback;
	public $alignment;
	public $gameplay;
	public $interrupt;
	public $igp;
	public $sound;
	public $iap;
	public $igpLocalization;
	public $localization;
	public $wjiap;
	public $mrc;
	public $tns;
	public $gc;
	public $authorId;
	public $createTime;
	public $updateTime;
	
	public $projectMemberId;

	public function rules()
	{
		return [
			[['id', 'projectId', 'buildId', 'feedback', 'alignment', 'gameplay', 'interrupt', 'igp', 'sound', 'iap', 'igpLocalization', 'localization', 'wjiap', 'mrc', 'tns', 'gc', 'authorId'], 'integer'],
			[['projectMemberId'], 'integer'],
			[['createTime', 'updateTime'], 'safe'],
		];
	}

	public function search($params)
	{
		$query = ProjectBuild::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['updateTime' => SORT_DESC],
			]
		]);

		if (!($this->load($params, '') && $this->validate())) {
			return $dataProvider;
		}

		$this->addCondition($query, 'id');
		$this->addCondition($query, 'projectId');
		$this->addCondition($query, 'buildId');
		$this->addCondition($query, 'feedback');
		$this->addCondition($query, 'alignment');
		$this->addCondition($query, 'gameplay');
		$this->addCondition($query, 'interrupt');
		$this->addCondition($query, 'igp');
		$this->addCondition($query, 'sound');
		$this->addCondition($query, 'iap');
		$this->addCondition($query, 'igpLocalization');
		$this->addCondition($query, 'localization');
		$this->addCondition($query, 'wjiap');
		$this->addCondition($query, 'mrc');
		$this->addCondition($query, 'tns');
		$this->addCondition($query, 'gc');
		$this->addCondition($query, 'authorId');
		$this->addCondition($query, 'createTime');
		$this->addCustomCondition($query, 'projectMemberId');
		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
	
	protected function addCustomCondition($query, $attribute)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		
		$builds = ProjectAssignment::find()
			->select('projectBuildId')
			->where(['projectMemberId' => $value])
			->column();
		$query->andWhere(['id' => $builds]);
	}
}

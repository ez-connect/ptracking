<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FoodMenu;

/**
 * FoodMenuSearch represents the model behind the search form about FoodMenu.
 */
class FoodMenuSearch extends Model
{
	public $id;
	public $foodStoreId;
	public $name;
	public $price;
	public $author;
	public $createTime;
	public $updateTime;

	public function rules()
	{
		return [
			[['id', 'foodStoreId', 'price', 'author'], 'integer'],
			[['name', 'createTime', 'updateTime'], 'safe'],
		];
	}
	
	public function search($params)
	{
		$query = FoodMenu::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['updateTime' => SORT_DESC],
			]
		]);

		if (!($this->load($params, '') && $this->validate())) {
			return $dataProvider;
		}

		$this->addCondition($query, 'id');
		$this->addCondition($query, 'foodStoreId');
		$this->addCondition($query, 'name', true);
		$this->addCondition($query, 'price');
		$this->addCondition($query, 'author');
		$this->addCondition($query, 'createTime');
		$this->addCondition($query, 'updateTime');
		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}

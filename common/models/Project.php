<?php

namespace common\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "Project".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $startDate
 * @property string $endDate
 * @property integer $foodStoreId
 * @property integer $authorId
 * @property string $createTime
 * @property string $updateTime
 *
 * @property FoodStore $foodStore
 * @property User $author
 * @property User $members
 * @property mixed $summary
 */
class Project extends \yii\db\ActiveRecord
{
	const PROJECT_ALL 		= 'all';
	const PROJECT_ACTIVE 	= 'active';
	
	private $_summary;
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'Project';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name', 'code', 'startDate', 'endDate', 'authorId'], 'required'],
			[['startDate', 'endDate', 'createTime', 'updateTime'], 'safe'],
			[['authorId', 'foodStoreId'], 'integer'],
			[['name'], 'string', 'max' => 255],
			[['code'], 'string', 'max' => 8],
			[['name', 'code'], 'unique'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Name',
			'code' => 'Code',
			'startDate' => 'Start',
			'endDate' => 'End',
			'foodStoreId' => 'Food Store',
			'authorId' => 'By',
			'createTime' => 'Create',
			'updateTime' => 'Update',
		];
	}

	/**
	 * Fill Project.authorId before call validate.
	 * @inheritdoc
	 */
	public function beforeValidate()
	{
		if ($this->isNewRecord) {
			$this->authorId = \Yii::$app->user->getId();
		}
		return parent::beforeValidate();
	}
	
	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getFoodStore()
	{
		return $this->hasOne(FoodStore::className(), ['id' => 'foodStoreId']);
	}
	
	/**
	 * Author, who created this project.
	 * @return \yii\db\ActiveRelation
	 */
	public function getAuthor()
	{
		return $this->hasOne(User::className(), ['id' => 'authorId']);
	}
	
	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getMembers()
	{
		return $this->hasMany(ProjectMember::className(), ['projectId' => 'id']);
	}
	
	/**
	 * Count some relative info.
	 * @return mixed
	 */
	public function getSummary()
	{
		if ($this->_summary == null) {
			$this->_summary = [
				'member' => ProjectMember::find()
					->where(['projectId' => $this->id])
					->count(),
				'build' => ProjectBuild::find()
					->where(['projectId' => $this->id])
					->count(),
				'unresolved-target' => Task::find()
					->where(['projectId' => $this->id, 'typeId' => TaskType::TYPE_TARGET])
					->andWhere(['statusId' => [TaskStatus::STATUS_NEW, TaskStatus::STATUS_ON_HOLD, TaskStatus::STATUS_OPEN]])
					->count(),
				'unresolved-issue' => Task::find()
					->where(['projectId' => $this->id, 'typeId' => TaskType::TYPE_ISSUE])
					->andWhere(['statusId' => [TaskStatus::STATUS_NEW, TaskStatus::STATUS_ON_HOLD, TaskStatus::STATUS_OPEN]])
					->count(),
				'unresolved-feedback' => $count = Task::find()
					->where(['projectId' => $this->id, 'typeId' => TaskType::TYPE_FEEDBACK])
					->andWhere(['statusId' => [TaskStatus::STATUS_NEW, TaskStatus::STATUS_ON_HOLD, TaskStatus::STATUS_OPEN]])
					->count(),
				
				'unresolved-milestone' => Task::find()
					->where(['projectId' => $this->id, 'typeId' => TaskType::TYPE_MILESTONE])
					->andWhere(['statusId' => [TaskStatus::STATUS_NEW, TaskStatus::STATUS_ON_HOLD, TaskStatus::STATUS_OPEN]])
					->count(),
			];
		}
		
		return $this->_summary;
	}
}

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\FoodMenu $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="food-menu-form">
	<?= $form->field($model, 'name')->textInput(['maxlength' => 64]) ?>
	<?= $form->field($model, 'price')->textInput() ?>
</div>

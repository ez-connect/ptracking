<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\FoodMenu $model
 */
?>

<?php $form = ActiveForm::begin([
	'options' => [
		'class' => 'form-horizontal',
	],
	'fieldConfig' => [
		'template' => "{label}\n<div class=\"col-lg-8\">{input}</div>\n<div class=\"col-lg-8 col-md-offset-3\">{error}</div>",
		'labelOptions' => ['class' => 'col-lg-3 control-label'],
	],
]); ?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4>Create</h4>
</div>
<div class="modal-body">
	<?php echo $this->render('_form', [
		'model' => $model,
		'form' => $form
	]); ?>
</div>
<div class="modal-footer clearfix">
	<?= Html::submitButton('Create', ['class' => 'btn btn-default']) ?>
	&nbsp;
	<?= Html::a('Cancel', '#', ['data-dismiss' =>'modal']) ?>
</div>
<?php ActiveForm::end(); ?>

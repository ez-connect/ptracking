<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\search\FoodMenuSearch $searchModel
 */

$this->title = 'Food Menus';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="food-menu-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a('Create Food Menu', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?php echo GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'id',
			'foodStoreId',
			'name',
			'price',
			'author',
			// 'createTime',
			// 'updateTime',

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>

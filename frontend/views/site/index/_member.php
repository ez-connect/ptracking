<?php

use ez\helpers\DateTime;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\User $model
 */
?>

<?= $model->name ?>&nbsp;
<span class="text-muted small">
	<?= DateTime::timeAgo($model->createTime) ?>
</span>

<?php

use ez\helpers\DateTime;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Project $model
 */
?>

<?php
$summary = $model->getSummary();
?>

<?= Html::a($model->name, ['project/view', 'id' => $model->id]) ?>&nbsp;
<span class="text-muted small"><?= DateTime::niceShort($model->endDate) ?></span>
<p class="text-muted small">
	<?= $summary['member'] ?> members &nbsp;&bull;&nbsp;
	<?= $summary['build'] ?> builds &nbsp;&bull;&nbsp;
	<?= $summary['unresolved-target'] ?> targets &nbsp;&bull;&nbsp;
	<?= $summary['unresolved-issue'] ?> issues &nbsp;&bull;&nbsp;
	<?= $summary['unresolved-feedback'] ?> feedbacks
</p>

<?php

use common\models\TaskStatus;
use ez\helpers\DateTime;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Task $model
 */
?>

<div><?= Html::a($model->name, ['task/view', 'id' => $model->id]) ?></div>
<p class="text-muted small">
	<?= $model->assignee->name ?>&nbsp;
	<?= TaskStatus::getLabel($model->statusId) ?>&nbsp;
	<?= DateTime::timeAgo($model->updateTime) ?>
</p>

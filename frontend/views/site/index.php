<?php

use common\models\Project;
use common\models\ProjectBuild;
use common\models\Task;
use common\models\TaskComment;
use common\models\TaskType;
use common\models\User;
use yii\helpers\Html;
use yii\widgets\ListView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $projectDataProvider
 * @var yii\data\ActiveDataProvider $targetDataProvider
 * @var yii\data\ActiveDataProvider $issueDataProvider
 * @var yii\data\ActiveDataProvider $feedbackDataProvider
 * @var yii\data\ActiveDataProvider $milestoneDataProvider
 * @var yii\data\ActiveDataProvider $memberDataProvider
 */
$this->title = 'Project Tracking';
?>

<?php
$projectCount = Project::find()->count();
$userCount = User::find()->count();
$buildCount = ProjectBuild::find()->count();
$taskCount = Task::find()->count();
$commentCount = TaskComment::find()->count();
?>

<div class="site-index">

    <div class="jumbotron">
        <h1><?= $projectCount ?> projects <small><?= $userCount ?> members</small></h1>

        <h2><?= $buildCount ?> <small>builds, </small><?= $taskCount ?> <small>tasks, and </small><?= $commentCount ?> <small>comments</small></h2>

        <p class="text-info">are holding</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h4>Recent projects</h4>
                <p>
                    <?= ListView::widget([
                        'dataProvider' => $projectDataProvider,
                        'itemView' => 'index/_project',
                        'layout' => "{items}\n"
                    ]); ?>
                </p>
                <p><?= Html::a('View all &raquo;', ['project/index'], ['class' => 'btn btn-default']) ?></p>
            </div>
            <div class="col-lg-4">
                <h4>Recent targets</h4>

                <p>
                    <?= ListView::widget([
                        'dataProvider' => $targetDataProvider,
                        'itemView' => 'index/_task',
                        'layout' => "{items}\n"
                    ]); ?>
                </p>
                <p><?= Html::a('View all &raquo;', '#task', ['class' => 'btn btn-default']) ?></p>
            </div>
            <div class="col-lg-4">
                <h4>Recent issues</h4>

                <p>
                    <?= ListView::widget([
                        'dataProvider' => $issueDataProvider,
                        'itemView' => 'index/_task',
                        'layout' => "{items}\n"
                    ]); ?>
                </p>
                <p><?= Html::a('View all &raquo;', '#issue', ['class' => 'btn btn-default']) ?></p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <h4>Recent feedbacks</h4>

                <p>
                    <?= ListView::widget([
                        'dataProvider' => $feedbackDataProvider,
                        'itemView' => 'index/_task',
                        'layout' => "{items}\n"
                    ]); ?>
                </p>
                <p><?= Html::a('View all &raquo;', '#feedback', ['class' => 'btn btn-default']) ?></p>
            </div>
            <div class="col-lg-4">
                <h4>Recent milestones</h4>

                <p>
                    <?= ListView::widget([
                        'dataProvider' => $milestoneDataProvider,
                        'itemView' => 'index/_task',
                        'layout' => "{items}\n"
                    ]); ?>
                </p>
                <p><?= Html::a('View all &raquo;', '#task', ['class' => 'btn btn-default']) ?></p>
            </div>
            <div class="col-lg-4">
                <h4>Recent members</h4>

                <p>
                    <?= ListView::widget([
                        'dataProvider' => $memberDataProvider,
                        'itemView' => 'index/_member',
                        'layout' => "{items}\n"
                    ]); ?>
                </p>
            </div>
        </div>

    </div>
</div>

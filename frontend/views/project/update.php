<?php

use common\models\FoodStore;
use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\Project $model
 */
?>

<?php $form = ActiveForm::begin([
    'id' => 'task-form',
    'options' => [
    	'class' => 'form-horizontal',
    	'enctype' => 'multipart/form-data'
    ],
    'fieldConfig' => [
		'template' => "{label}\n<div class=\"col-lg-8\">{input}</div>\n<div class=\"col-lg-8 col-md-offset-3\">{error}</div>",
		'labelOptions' => ['class' => 'col-lg-3 control-label'],
    ],
]); ?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4>Update</h4>
</div>
<div class="modal-body">
	<?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
		
	<?= $form->field($model, 'code')->textInput(['maxlength' => 8]) ?>

	<?= $form->field($model,'startDate')->widget(DatePicker::className(), [
		'options' => ['placeholder' => 'Enter start date ...'],
		'pluginOptions' => [
			'autoclose' => true,
			'format' => 'yyyy-mm-dd'
		]
	]) ?>

	<?= $form->field($model,'endDate')->widget(DatePicker::className(), [
		'type' => DatePicker::TYPE_COMPONENT_APPEND,
		'options' => ['placeholder' => 'Enter end date ...'],
		'pluginOptions' => [
			'autoclose' => true,
			'format' => 'yyyy-mm-dd'
		]
	]) ?>
	
	<?= $form->field($model, 'foodStoreId')->dropDownList(FoodStore::getArray()) ?>
</div>
<div class="modal-footer">
	<?= Html::submitButton('Update', ['class' => 'btn btn-default']) ?>
	&nbsp;
	<?= Html::a('Cancel', '#', ['data-dismiss' =>'modal']) ?>
</div>

<?php ActiveForm::end(); ?>

<?php

use common\models\Task;
use ez\widgets\AmChart;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Project $model
 * @var integer $typeId
 * @var string $chartId
 */
?>

<?= AmChart::widget([
	'id' => $chartId,
	'width' => '100%',
	'height' => '300px',
	'chart' => [
		'type' => 'serial',
		'dataProvider' => [
			Task::getSummary($model->id, $typeId, date('y-m-d', strtotime("-6 days"))),
			Task::getSummary($model->id, $typeId, date('y-m-d', strtotime("-5 days"))),
			Task::getSummary($model->id, $typeId, date('y-m-d', strtotime("-4 days"))),
			Task::getSummary($model->id, $typeId, date('y-m-d', strtotime("-3 days"))),
			Task::getSummary($model->id, $typeId, date('y-m-d', strtotime("-2 days"))),
			Task::getSummary($model->id, $typeId, date('y-m-d', strtotime("-1 days"))),
			Task::getSummary($model->id, $typeId, date('y-m-d')),
		],
		'dataDateFormat' => 'YYYY-MM-DD',
		'categoryField' => 'date',
		'startDuration' => 1,
		'columnWidth' => 0.2,
		'chartCursor' => [
			'cursorPosition' => 'mouse'
		],
		
		'categoryAxis' => ['gridPosition' => 'start'],
		'valueAxes' => [['axisAlpha' => 0.2]],
		'graphs' => [
			[
				'valueField' => 'total',
				'type' => 'column',
				'title' => 'Total',
				'fillAlphas' => 1,
				'balloonText' => '[[value]] total'
			],
			[
				'valueField' => 'closed',
				'title' => 'Closed',
				'bullet' => 'round',
				'balloonText' => '[[value]]<b> closed</b>'
			],
		]
	],
]); ?>

<?php

use common\models\TaskStatus;
use common\models\TaskType;
use ez\helpers\DateTime;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var common\models\Project $model
 */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('//layouts/_navbar', ['project' => $model, 'controller' => 'project']) ?>

<div class="panel panel-default small">
	<div class="panel-body">
		<div class="col-md-6">
			<?php echo DetailView::widget([
				'model' => $model,
				'attributes' => [
					[
						'label' => 'name',
						'format' => 'raw',
						'value' => '<strong>' .$model->name . '</strong>'
					],
					[
						'label' => 'kick off',
						'format' => 'raw',
						'value' => '<span class="text-info">' . DateTime::niceShort($model->startDate) . '</span>'
					],
					[
						'label' => 'deadline',
						'format' => 'raw',
						'value' => '<span class="text-danger">' . DateTime::niceShort($model->endDate) . '</span>'
					],
					[
						'label' => 'member',
						'value' => $model->summary['member']
					],
					[
						'label' => 'build',
						'value' => $model->summary['build']
					],
				],
			]); ?>
		</div>
		<div class="col-md-6">
			<?php echo DetailView::widget([
				'model' => $model,
				'attributes' => [
					[
						'label' => 'target',
						'format' => 'raw',
						'value' => Html::a($model->summary['unresolved-target'] . ' unresolved', ['task/index',
							'projectId' => $model->id,
							'typeId' => TaskType::TYPE_TARGET,
							'status' => TaskStatus::STATUS_UNRESOLVED
						])
					],
					[
						'label' => 'issue',
						'format' => 'raw',
						'value' => Html::a($model->summary['unresolved-issue'] . ' unresolved', ['task/index',
							'projectId' => $model->id,
							'typeId' => TaskType::TYPE_ISSUE,
							'status' => TaskStatus::STATUS_UNRESOLVED
						])
					],
					[
						'label' => 'feedback',
						'format' => 'raw',
						'value' => Html::a($model->summary['unresolved-feedback'] . ' unresolved', ['task/index',
							'projectId' => $model->id,
							'typeId' => TaskType::TYPE_FEEDBACK,
							'status' => TaskStatus::STATUS_UNRESOLVED
						])
					],
					[
						'label' => 'milestone',
						'format' => 'raw',
						'value' => Html::a($model->summary['unresolved-milestone'] . ' unresolved', ['task/index',
							'projectId' => $model->id,
							'typeId' => TaskType::TYPE_MILESTONE,
							'status' => TaskStatus::STATUS_UNRESOLVED
						])
					],
				],
			]); ?>
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">Timeline</div>
	<div class="panel-body">
		<?= $this->render('_charts/_timeline', ['model' => $model]) ?>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">Task</div>
	<div class="panel-body">
		<?= $this->render('_charts/_task', [
			'model' => $model,
			'typeId' => TaskType::TYPE_TARGET,
			'chartId' => 'chart-task',
		]) ?>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">Issue</div>
	<div class="panel-body">
		<?= $this->render('_charts/_task', [
			'model' => $model,
			'typeId' => TaskType::TYPE_ISSUE,
			'chartId' => 'chart-issue',
		]) ?>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading">Feedback</div>
	<div class="panel-body">
		<?= $this->render('_charts/_task', [
			'model' => $model,
			'typeId' => TaskType::TYPE_FEEDBACK,
			'chartId' => 'chart-feedback',
		]) ?>
	</div>
</div>

<?php

use ez\helpers\DateTime;
use ez\widgets\Modal;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var common\models\FoodStore $model
 */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Dash board', 'url' => ['dash-board/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h3><?= Html::encode($this->title) ?></h3>

<?php echo DetailView::widget([
	'model' => $model,
	'attributes' => [
		'name',
		'contact',
		'address',
		'tel',
		'mobile',
	],
]); ?>

<?php
Modal::begin([
	'clearData' => true,
	'id' => 'popup-modal',
	'clientOptions' => [
		'modal' => true,
		'autoOpen' => false,
	],
]);
Modal::end();
?>

<?php echo GridView::widget([
	'dataProvider' => $dataProvider,
	'tableOptions' => [
		'class' => 'table table-hover',
	],
	'columns' => [
		['class' => 'yii\grid\SerialColumn'],
		
		'name',
		'price',
		[
			'attribute' => 'updateTime',
			'value' => function ($model, $index, $widget) {
				return DateTime::timeAgo($model->updateTime);
			}
		],
		[
			'attribute' => 'authorId',
			'value' => function ($model, $index, $widget) {
				return $model->author->name;
			}
		],
		[
			'label' => '',
			'format' => 'raw',
			'value' => function ($model, $index, $widget) {
				$updateUrl = Html::a('<span class="glyphicon glyphicon-pencil"></span> ',
					['food-menu/update', 'id' => $model->id],
					[
						'class' => 'tool-tip',
						'title' => 'Update',
						'data-toggle' => 'modal',
						'data-target' => '#popup-modal',
					]
				);
				$deleteUrl = Html::a('<span class="glyphicon glyphicon-trash"></span> ',
					['food-menu/delete', 'id' => $model->id],
					[
						'class' => 'tool-tip',
						'title' => 'Delete',
						'data-toggle' => 'modal',
						'data-target' => '#popup-modal',
					]
				);
				
				return ($updateUrl . $deleteUrl);
			}
		],
	]
]); ?>

<?= Html::a('Add', ['food-menu/create', 'foodStoreId' => $model->id], [
	'class' => 'btn btn-default pull-right',
	'data-toggle' => 'modal',
	'data-target' => '#popup-modal',
]) ?>

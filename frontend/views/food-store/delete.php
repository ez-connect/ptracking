<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\FoodStore $model
 * use yii\widgets\ActiveForm;
 */
?>

<?php ActiveForm::begin(); ?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4>Delete</h4>
</div>
<div class="modal-body">
	<p><strong><?= $model->name ?></strong> will be removed?</p>
</div>
<div class="modal-footer clearfix">
	<?= Html::submitButton('Yes', ['class' => 'btn btn-default']) ?>
	&nbsp;
	<?= Html::a('Cancel', '#', ['data-dismiss' =>'modal']) ?>
</div>
<?php ActiveForm::end(); ?>

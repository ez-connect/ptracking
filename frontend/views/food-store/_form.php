<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\FoodStore $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="food-store-form">

	<?= $form->field($model, 'name')->textInput(['maxlength' => 64]) ?>
	
	<?= $form->field($model, 'contact')->textInput(['maxlength' => 64]) ?>
	
	<?= $form->field($model, 'address')->textInput(['maxlength' => 64]) ?>
	
	<?= $form->field($model, 'tel')->textInput(['maxlength' => 64]) ?>
	
	<?= $form->field($model, 'mobile')->textInput(['maxlength' => 64]) ?>
	
</div>

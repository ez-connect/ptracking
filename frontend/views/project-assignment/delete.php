<?php

use common\models\Task;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\ProjectAssignment $model
 */
?>

<?php ActiveForm::begin(); ?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4>Are you sure</h4>
</div>
<div class="modal-body">
	Delete the assignment of <code><?= $model->projectMember->member->name ?></code> 
	that cause all tasks of his/her will be deleted also.<br />
</div>
<div class="modal-footer">
	<?= Html::submitButton('Yes', ['class' => 'btn btn-default']) ?>
	&nbsp;
	<?= Html::a('No', '#', ['data-dismiss' =>'modal']) ?>
</div>
<?php ActiveForm::end(); ?>

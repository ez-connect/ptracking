<?php

use ez\helpers\DateTime;
use ez\helpers\JSRegister;
use ez\widgets\LiveFilter;
use ez\widgets\Modal;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\Project $project
 */

$this->title = $project->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['project/index']];
$this->params['breadcrumbs'][] = $this->title;

JSRegister::registerTooltip($this, '.initialism');
?>

<?= $this->render('//layouts/_navbar', ['project' => $project, 'controller' => 'project-assignment']) ?>

<?php
// Create a Modal to delete the a row
Modal::begin([
	'id' => 'delete-modal',
	'clearData' => true,
	'clientOptions' => [
		'modal' => true,
		'autoOpen' => false,
	],
]);
Modal::end();
?>

<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<strong>Assigment management</strong>
		<div class="col-sm-3 col-md-3 pull-right">
		<?=	LiveFilter::widget([
			'name' => 'filter',
			'selection' => '.grid-view tbody tr',
			'options' => [
				'placeholder' => 'Search...',
			]
		]);	?>
		</div>
	</div>
	
	<?= $this->render('_create', ['project' => $project]) ?>

	<div class="panel-body">	
		<?php echo GridView::widget([
			'dataProvider' => $dataProvider,
			'tableOptions' => [
				'class' => 'table table-hover',
			],
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],

				[
					'attribute' => 'projectMemberId',
					'value' => function($model, $index, $widget) {
						return $model->projectMember->member->name;
					}
				],
				[
					'attribute' => 'projectBuildId',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a($model->projectBuild->build->code, '#', [
							'class' => 'initialism',
							'data-toggle' => 'tooltip',
							'title' => $model->projectBuild->build->name
						]);
					}
				],
				[
					'attribute' => 'authorId',
					'value' => function($model, $index, $widget) {
						return $model->author->name;
					}
				],
				[
					'attribute' => 'createTime',
					'value' => function($model, $index, $widget) {
						return DateTime::timeAgo($model->createTime);
					}
				],
				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{delete}',
					'buttons' => [
						'delete' => function ($url, $model) {
							return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
								'data-toggle' => 'modal',
								'data-target' => '#delete-modal'
							]);
						}
					],
				],
			],
		]); ?>
	</div>
</div>

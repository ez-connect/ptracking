<?php

use ez\widgets\LiveFilter;
use ez\widgets\Modal;
use ez\helpers\DateTime;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\Project $project
 * @var yii\db\ActiveDataProvider $dataProvider
 */

$this->title = $project->name;
$this->params['breadcrumbs'][] = ['label' => $project->name, 'url' => ['project/index', 'projectId' => $project->id]];
$this->params['breadcrumbs'][] = 'Build';
?>

<?= $this->render('//layouts/_navbar', ['project' => $project, 'controller' => 'project-build']) ?>

<?php
Modal::begin([
	'id' => 'delete-modal',
	'clearData' => true,
	'clientOptions' => [
		'modal' => true,
		'autoOpen' => false,
	],
]); 
Modal::end();
?>

<div class="panel panel-default">
	<div class="panel-footer clearfix">
		<div class="col-sm-3 col-md-3 pull-right">
		<?=	LiveFilter::widget([
			'name' => 'filter',
			'selection' => '.grid-view tbody tr',
			'options' => [
				'placeholder' => 'Search...',
			]
		]);	?>
		</div>
	</div>
	
	<?= $this->render('_create', ['project' => $project]) ?>
	
	<div class="panel-body">
		<?php echo GridView::widget([
			'dataProvider' => $dataProvider,
			'tableOptions' => [
				'class' => 'table table-hover',
			],
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],

				[
					'attribute' => 'buildId',
					'value' => function($model, $index, $widget) {
						return $model->build->name;
					}
				],
				[
					'attribute' => 'authorId',
					'value' => function($model, $index, $widget) {
						return $model->author->name;
					}
				],
				[
					'attribute' => 'createTime',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return DateTime::timeAgo($model->createTime);
					}
				],
				
				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{delete}',
					'buttons' => [
						'delete' => function ($url, $model) {
							return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
								'data-toggle' => 'modal',
								'data-target' => '#delete-modal'
							]);
						}
					],
				],
			],
		]); ?>
	</div>
</div>

<?php

use yii\bootstrap\Collapse;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\ProjectBuild $model
 * @var yii\data\ActiveDataProvider $targetProvider
 * @var yii\data\ActiveDataProvider $feedbackProvider
 * @var yii\data\ActiveDataProvider $issueProvider
 */

$this->title = $model->project->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['project/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('//layouts/_navbar', ['project' => $model->project, 'controller' => 'project-build']) ?>

<div class="row">
    <div class="col-md-10">
		<?= $this->render('view/_info', ['model' => $model]) ?>
		<?php echo Collapse::widget([
			'items' => [
				'Target' => [
					'content' => $this->render('view/_task', ['model' => $model, 'dataProvider' => $targetProvider]),
					// open its content by default
					'contentOptions' => ['class' => 'in']
				],
				'Issue' => [
					'content' => $this->render('view/_task', ['model' => $model, 'dataProvider' => $issueProvider]),
					'contentOptions' => ['class' => 'out'],
					'options' => [],
				],
				'Feedback' => [
					'content' => $this->render('view/_task', ['model' => $model, 'dataProvider' => $feedbackProvider]),
					'contentOptions' => ['class' => 'out']
				],
			]
		]); ?>
    </div>
	<div class="col-md-2">
		<div class="pull-right">
			<?= $this->render('view/_status', ['model' => $model]) ?>
		</div>
	</div>
</div>

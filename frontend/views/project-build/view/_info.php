<?php

use common\models\BuildStatus;
use common\models\TaskType;
use common\models\Role;
use ez\widgets\LiveFilter;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\ProjectBuild $model
 */
?>

<?php
$assignments = $model->assignees;
?>

<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<strong><?= $model->build->name ?><span class="small"> <?= BuildStatus::getLabel($model->gc) ?></span></strong>
		<div class="col-md-4 pull-right">
			<?=	LiveFilter::widget([
				'name' => 'filter',
				'selection' => '.grid-view tbody tr',
				'options' => [
					'placeholder' => 'Search...',
				]
			]);	?>
		</div>
	</div>
	<div class="panel-body">
		<?php foreach ($assignments as $assignment): ?>
			<?= $assignment->projectMember->member->getAvatarImg(16, 16) ?>
			&nbsp;
			<?= Html::a($assignment->projectMember->member->name, '#') ?>
			<span class="text-muted small"><?= Role::getName($assignment->projectMember->member->role) ?></span>
			&nbsp;
		<?php endforeach ?>
		
		<ul>
			<li>
				<?= $model->summary['unresolved-target'] ?> unresolved targets
				<?= Html::a(' <span class="glyphicon glyphicon-plus small"></span>',
					[
						'task/create',
						'projectId' => $model->projectId,
						'typeId' => TaskType::TYPE_TARGET,
						'projectBuildId' => $model->id,
					],
					[
						'class' => 'tip',
						'data-toggle' => 'tooltip',
						'title' => 'Add target...'
					]
				) ?>
			</li>
			<li>
				<?= $model->summary['unresolved-issue'] ?> unresolved issues
				<?= Html::a(' <span class="glyphicon glyphicon-plus small"></span>',
					[
						'task/create',
						'projectId' => $model->projectId,
						'typeId' => TaskType::TYPE_ISSUE,
						'projectBuildId' => $model->id,
					],
					[
						'class' => 'tip',
						'data-toggle' => 'tooltip',
						'title' => 'Add issue...'
					]
				) ?>
			</li>
			<li>
				<?= $model->summary['unresolved-feedback'] ?> unresolved feedbacks
				<?= Html::a(' <span class="glyphicon glyphicon-plus small"></span>',
					[
						'task/create',
						'projectId' => $model->projectId,
						'typeId' => TaskType::TYPE_FEEDBACK,
						'projectBuildId' => $model->id,
					],
					[
						'class' => 'tip',
						'data-toggle' => 'tooltip',
						'title' => 'Add feedback...'
					]
				) ?>
			</li>
			<li>
				<?= $model->summary['unresolved-milestone'] ?> unresolved milestones
				<?= Html::a(' <span class="glyphicon glyphicon-plus small"></span>',
					[
						'task/create',
						'projectId' => $model->projectId,
						'typeId' => TaskType::TYPE_MILESTONE,
						'projectBuildId' => $model->id,
					],
					[
						'class' => 'tip',
						'data-toggle' => 'tooltip',
						'title' => 'Add milestone...'
					]
				) ?>
			</li>
		</ul>
	</div>
</div>

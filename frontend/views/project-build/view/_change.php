<?php

use common\models\BuildStatus;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\ProjectBuild $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<?php
$status = BuildStatus::getArray();
?>

<?php $form = ActiveForm::begin([
	'id' => 'change-form',
	// 'enableAjaxValidation' => true,
	'action' =>['project-build/update', 'id' => $model->id],
]); ?>

<div class="modal fade" id="change-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 id="change-modal-title">Change status</h4>
			</div>
			<div class="modal-body">
				<div class="row form-horizontal">
					<label class="col-lg-2 control-label">To</label>
					<div class="col-lg-10">
						<?= Html::dropDownList('change-attribute', 2, $status, [
							'id' => 'change-attribute',
							'class' => 'form-control'
						]) ?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<?= Html::submitButton('Change', ['id' => 'change-submit', 'class' => 'btn btn-default']) ?>
				&nbsp;
				<?= Html::a('Cancel', '#', ['data-dismiss' => 'modal']) ?>
			</div>
		</div>
	</div>
</div>

<?php ActiveForm::end(); ?>

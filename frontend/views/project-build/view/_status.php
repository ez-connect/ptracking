<?php

use common\models\BuildStatus;
use ez\helpers\DateTime;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var common\models\ProjectBuild $model
 */
?>

<?= $this->render('view/_change', ['model' => $model]) ?>

<div class="panel panel-default">
	<?= DetailView::widget([
		'model' => $model,
		'template' => "<tr class=\"row\">\n<td class=\"col-lg-8 text-right\">{label}</td>\n<td class=\"col-lg-4\">{value}</td>\n</tr>\n",
		'options' => [
			'class' => 'table table-hover small',
		],
		'attributes' => [
			[
				'label' => 'feedback',
				'format' => 'raw',
				'value' => Html::a(BuildStatus::getLabel($model->feedback), '#status', [
					'class' => 'build-change-status',
					'title' => 'Change feedback',
					'data-attribute' => 'feedback',
					'data-status' => $model->feedback
				])
			],
			[
				'label' => 'alignment',
				'format' => 'raw',
				'value' => Html::a(BuildStatus::getLabel($model->alignment), '#status', [
					'class' => 'build-change-status',
					'title' => 'Change alignment',
					'data-attribute' => 'alignment',
					'data-status' => $model->alignment
				])
			],
			[
				'label' => 'game-play',
				'format' => 'raw',
				'value' => Html::a(BuildStatus::getLabel($model->gameplay), '#status', [
					'class' => 'build-change-status',
					'title' => 'Change game-play',
					'data-attribute' => 'gameplay',
					'data-status' => $model->gameplay
				])
			],
			[
				'label' => 'interrupt',
				'format' => 'raw',
				'value' => Html::a(BuildStatus::getLabel($model->interrupt), '#status', [
					'class' => 'build-change-status',
					'title' => 'Change interrupt',
					'data-attribute' => 'interrupt',
					'data-status' => $model->interrupt
				])
			],
			[
				'name' => 'igp',
				'format' => 'raw',
				'value' => Html::a(BuildStatus::getLabel($model->igp), '#status', [
					'class' => 'build-change-status',
					'title' => 'Change igp',
					'data-attribute' => 'igp',
					'data-status' => $model->igp
				])
			],
			[
				'label' => 'sound',
				'format' => 'raw',
				'value' => Html::a(BuildStatus::getLabel($model->sound), '#status', [
					'class' => 'build-change-status',
					'title' => 'Change sound',
					'data-attribute' => 'sound',
					'data-status' => $model->sound
				])
			],
			[
				'name' => 'iap',
				'format' => 'raw',
				'value' => Html::a(BuildStatus::getLabel($model->iap), '#status', [
					'class' => 'build-change-status',
					'title' => 'Change IAP',
					'data-attribute' => 'iap',
					'data-status' => $model->iap
				])
			],
			[
				'label' => 'igp loc',
				'format' => 'raw',
				'value' => Html::a(BuildStatus::getLabel($model->igpLocalization), '#status', [
					'class' => 'build-change-status',
					'title' => 'Change IGP Localization',
					'data-attribute' => 'igpLocalization',
					'data-status' => $model->igpLocalization
				])
			],
			[
				'label' => 'localization',
				'format' => 'raw',
				'value' => Html::a(BuildStatus::getLabel($model->localization), '#status', [
					'class' => 'build-change-status',
					'title' => 'Change localization',
					'data-attribute' => 'localization',
					'data-status' => $model->localization
				])
			],
			[
				'name' => 'wjiap',
				'format' => 'raw',
				'value' => Html::a(BuildStatus::getLabel($model->wjiap), '#status', [
					'class' => 'build-change-status',
					'title' => 'Change WJIAP',
					'data-attribute' => 'wjiap',
					'data-status' => $model->wjiap
				])
			],
			[
				'name' => 'mrc',
				'format' => 'raw',
				'value' => Html::a(BuildStatus::getLabel($model->mrc), '#status', [
					'class' => 'build-change-status',
					'title' => 'Change MRC',
					'data-attribute' => 'mrc',
					'data-status' => $model->mrc
				])
			],
			[
				'name' => 'tns',
				'format' => 'raw',
				'value' => Html::a(BuildStatus::getLabel($model->tns), '#status', [
					'class' => 'build-change-status',
					'title' => 'Change TNS',
					'data-attribute' => 'tns',
					'data-status' => $model->tns
				])
			],
			[
				'label' => 'gold check',
				'format' => 'raw',
				'value' => Html::a(BuildStatus::getLabel($model->gc), '#status', [
					'class' => 'build-change-status',
					'title' => 'Change gold check',
					'data-attribute' => 'gc',
					'data-status' => $model->gc
				])
			],
		],
	]); ?>
</div>

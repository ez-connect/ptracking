<?php

use common\models\Build;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\Project $project
 */

$builds = Build::getArrayByProjectId($project->id);
?>

<?php if (count($builds) > 0): ?>
<?php $form = ActiveForm::begin(['action' =>['project-build/create', 'projectId' => $project->id]]); ?>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-lg-6">
				<div class="input-group">
					<?=	Select2::widget([
						'name' => 'builds',
						'data' => $builds,
						'options' => [
							'placeholder' => 'Select one or more builds to add...',
							'multiple' => true
						],
					]);	?>
					
					<span class="input-group-btn">
						<?= Html::submitButton('Add', ['type' => 'button', 'data-loading-text' => 'Please wait...', 'class' => 'btn btn-primary']) ?>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>
<?php ActiveForm::end(); ?>
<?php endif ?>

<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\Task $model
 */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4>Are you sure</h4>
</div>
<div class="modal-body">
	Do you want to delete this <?= $model->type ?>?
</div>
<div class="modal-footer">
	<?= Html::submitButton('Yes', ['class' => 'btn btn-default']) ?>
	&nbsp;
	<?= Html::a('No', '#', ['data-dismiss' =>'modal']) ?>
</div>
<?php ActiveForm::end(); ?>

<?php

use common\models\Priority;
use common\models\Task;
use common\models\TaskStatus;
use common\models\TaskType;
use common\models\search\TaskSearch;
use ez\helpers\DateTime;
use ez\widgets\LiveFilter;
use ez\widgets\Modal;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\Project $project
 * @var integer $typeId
 * @var string $filter
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = $project->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['project/index']];
$this->params['breadcrumbs'][] = $this->title;

// Load Email action via AJAX
Modal::begin([
	'id' => 'task-email-modal',
	'clientOptions' => [
		'modal' => true,
		'autoOpen' => false,
	],
]);
Modal::end();
?>

<?= $this->render('//layouts/_navbar', ['project' => $project, 'controller' => 'task', 'typeId' => $typeId]) ?>

<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<div class="row">
			<div class="col-md-2">
				<?= Html::a('New ' . TaskType::getName($typeId),  ['create', 'projectId' => $project->id, 'typeId' => $typeId], [
					'class' => 'btn btn-default'
				]) ?>
			</div>
			<div class="col-md-10">
				<?= $this->render('_filter', ['project' => $project, 'typeId' => $typeId, 'filter' => $filter]) ?>
				<div class="col-md-4 pull-right">
				<?=	LiveFilter::widget([
					'name' => 'filter',
					'selection' => '.grid-view tbody tr',
					'options' => [
						'placeholder' => 'Search...',
					]
				]);	?>
				</div>
			</div>
		</div>
	</div>
	<div class="panel-body">
		<?php /* Pjax::begin([]); */ ?>
		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'tableOptions' => [
				'class' => 'table table-hover',
			],
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],

				[
					'attribute' => 'name',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						//$name = $model->name;
						//if ($model->statusId == TaskStatus::STATUS_RESOLVED) {
						//	$name = '<strike>' . $name . '</strike>';
						//}
						return Html::a($model->name, ['task/view', 'id' => $model->id]);
					}
				],
				[
					'attribute' => 'statusId',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a(TaskStatus::getLabel($model->statusId), ['index',
							'projectId' => $model->project->id,
							'typeId' => $model->typeId,
							'statusId' => $model->statusId
						]);
					}
				],
				[
					'attribute' => 'priorityId',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a(Priority::getText($model->priorityId), ['index',
							'projectId' => $model->project->id,
							'typeId' => $model->typeId,
							'priorityId' => $model->priorityId
						]);
					}
				],
				[
					'attribute' => 'assigneeId',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a($model->assignee->name, ['index',
							'projectId' => $model->project->id,
							'typeId' => $model->typeId,
							'assigneeId' => $model->assigneeId
						]);
					}
				],
				[
					'attribute' => 'Build',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a($model->projectBuild->build->code, ['index',
							'projectId' => $model->project->id,
							'typeId' => $model->typeId,
							'projectBuildId' => $model->projectBuildId
						]);
					}
				],
				[
					'attribute' => 'endDate',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a(DateTime::niceShort($model->endDate), ['index',
							'projectId' => $model->project->id,
							'typeId' => $model->typeId,
							'endDate' => $model->endDate
						]);
					}
				],
				[
					'attribute' => 'updateTime',
					'value' => function($model, $index, $widget) {
						return DateTime::timeAgo($model->updateTime);
					}
				],
			],
		]); ?>
		<?php /* Pjax::end(); */ ?>
	</div>
	<div class="panel-footer clearfix">
		<?php
			$params = Yii::$app->getRequest()->getQueryParams();
			$action[] = 'mail';				// Can't use ArrayHelper::merge() method
			foreach ($params as $key => $value) {
				$action[$key] = $value;
			}
			
			echo Html::a('Email', $action, [
				'class' => 'btn btn-default pull-right', 'data-toggle' => 'modal', 'data-target' => '#task-email-modal'
			]);
		?>
	</div>
</div>

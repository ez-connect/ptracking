<?php

use common\models\Priority;
use common\models\TaskStatus;
use common\models\TaskType;
use ez\helpers\DateTime;
use yii\helpers\Html;
use yii\helpers\Markdown;

/**
 * @var yii\web\View $this
 * @var common\models\Task $task
 * @var common\models\TaskComment $model
 */
?>

<div class="comment-item">
	<?= $model->author->getAvatarImg() ?>&nbsp;
	<strong><?= $model->author->name ?></strong>
	<ul>
		<?php if ($model->projectBuildId != null): ?>
			<li>
			<em>changed build to </em><?= Html::a($model->projectBuild->build->code, '#') ?>
			</li>
		<?php endif ?>
		<?php if ($model->assigneeId != null): ?>
			<li>
			<em>changed assignee to </em><?= Html::a($model->assignee->name, '#') ?>
			</li>
		<?php endif ?>
		<?php if ($model->typeId != null): ?>
			<li>
			<em>changed type to </em><?= TaskType::getName($model->typeId) ?>
			</li>
		<?php endif ?>
		<?php if ($model->statusId != null): ?>
			<li>
			<em>changed status to </em><?= TaskStatus::getLabel($model->statusId) ?>
			</li>
		<?php endif ?>
		<?php if ($model->name != null): ?>
			<li>
			<em>changed name to </em><?= Html::a($model->name, '#') ?>
			</li>
		<?php endif ?>
		<?php if ($model->isDescriptionChanged): ?>
			<li>
			<em>changed description.</em>
			</li>
		<?php endif ?>
		<?php if ($model->priorityId != null): ?>
			<li>
			<em>changed priority to </em><?= Priority::getLabel($model->priorityId) ?>
			</li>
		<?php endif ?>
		<?php if ($model->startDate != null): ?>
			<li>
			<em>changed start date to </em><?= $model->startDate ?>
			</li>
		<?php endif ?>
		<?php if ($model->endDate != null): ?>
			<li>
			<em>changed end date to </em><?= $model->endDate ?>
			</li>
		<?php endif ?>
	</ul>
	<?= Markdown::process($model->comment) ?>
	<small class="text-muted"><?= DateTime::timeAgo($model->createTime) ?></small>
</div>
<?php

use common\models\TaskStatus;
use ez\helpers\DateTime;
use kartik\markdown\Markdown;
use yii\helpers\Html;
// use yii\helpers\Markdown;

/**
 * @var yii\web\View $this
 * @var common\models\Task $model
 */

$attachments = $model->attachments;
?>

<p><?= ucfirst($model->type) . ' #'. $model->id . ' ' . TaskStatus::getLabel($model->statusId) ?></p>
<div class="task-info">
	<h4><?= $model->name ?><span class="text-muted"> #<?= $model->id ?></span></h4>
	<p>
		<?= $model->author->getAvatarImg() ?>
		<strong>
			<?= Html::a($model->author->name, ['task/index',
				'projectId' => $model->projectId,
				'typeId' => $model->typeId,
				'authorId' => $model->authorId
			]) ?>
		</strong> 
		created this 
		<?= $model->type ?> 
		<small class="text-muted"><?= DateTime::timeAgo($model->createTime) ?></small>
	</p>
	<?php
		$description = ($model->description != null) ? $model->description : '*No description provided.*';
		echo Markdown::process($description);
	?>
	
	<?php if (count($attachments) > 0): ?>
	<div class="row">
		<?php foreach ($attachments as $attachment): ?>
		<div class="col-md-3">
			<span class="glyphicon glyphicon-paperclip"></span>&nbsp;
			<?= Html::a($attachment->baseName, $attachment->getAttachmentPath(), [
				'target' => '_blank',
			]) ?>
			<?= Html::a('&times;', '#delete-attachment', [
				'class' => 'task-change',
				'data-title' => 'Delete attachment',
				'data-action' => Html::url(['task-attachment/delete', 'id' => $attachment->id]),
				'data-submit-label' => 'Delete'
			]) ?>
		</div>
		<?php endforeach ?>
	</div>
	<?php endif ?>
</div>

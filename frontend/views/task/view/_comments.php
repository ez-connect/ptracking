<?php

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\ListView;

/**
 * @var yii\web\View $this
 * @var common\models\Task $model
 */

$dataProvider = $model->getComments();
?>

<h4>Comments (<?= count($dataProvider->getModels()) ?>)</h4>

<div class="comment-item">
	<?= $model->author->getAvatarImg() ?>&nbsp;
	<?= Html::a('Leave your comment',	'#comment', [
		'class' => 'task-change',
		'data-title' => 'Leave your comment',
		'data-submit-label' => 'Comment'
	]); ?>
</div>

<div class="comment-list">
	<?= ListView::widget([
		'viewParams' => ['task' => $model],
		'dataProvider' => $dataProvider,
		'itemView' => 'view/_comment',
		'layout' => "{items}\n"
	]); ?>
</div>

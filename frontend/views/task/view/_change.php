<?php

use common\models\Task;
use common\models\TaskAttachment;
use common\models\TaskComment;
use common\models\TaskStatus;
use kartik\markdown\MarkdownEditor;
use kartik\widgets\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\Task $model
 * @var yii\widgets\ActiveForm $form
 */

$attachment = new TaskAttachment();
$comment = new TaskComment();
?>

<?php $form = ActiveForm::begin([
	'id' => 'change-form',
	'action' => ['task/update', 'id' => $model->id],
    'options' => [
    	'class' => 'form-horizontal',
    	'enctype' => 'multipart/form-data'
    ],
    'fieldConfig' => [
		'template' => "{label}\n<div class=\"col-lg-10\">{input}</div>\n<div class=\"col-lg-10 col-md-offset-2\">{error}</div>",
		'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>

<div class="modal fade" id="change-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 id="change-modal-title">Title</h4>
			</div>
			<div class="modal-body">
				<div class="row" id="change-status">
					<?= Html::activeHiddenInput($model, 'statusId') ?>
				</div>
				
				<?= $form->field($attachment, 'baseName')->widget(FileInput::className(), [
					'showUpload' => false,
					'showRemove' => false,
					'options' => [
						// 'multiple' => true,
					],
					'buttonOptions' => [
						'class' => 'btn btn-default',
					],
				]) ?>
				
				<?= $form->field($comment, 'comment')->widget(MarkdownEditor::className(), [
					'height' => 80,
					'toolbar' => [
						[
							'buttons' => [],
						],
					],
					'footerMessage' => 'For format your input, see the ' . Html::a('Markdown Syntax', 'http://michelf.ca/projects/php-markdown/concepts/'),
					'showExport' => false,
				]) ?>
			</div>
			<div class="modal-footer">
				<?= Html::submitButton('Change', ['id' => 'change-primary-btn', 'class' => 'btn btn-default']) ?>
				&nbsp;
				<?= Html::a('Cancel', '#', ['data-dismiss' => 'modal']) ?>
			</div>
		</div>
	</div>
</div>

<?php ActiveForm::end(); ?>

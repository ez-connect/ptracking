<?php

use common\models\TaskStatus;
use ez\helpers\DateTime;
use kartik\markdown\Markdown;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Task $model
 */

$this->title = $model->project->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['project/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('//layouts/_navbar', ['project' => $model->project, 'controller' => 'task', 'typeId' => $model->typeId]) ?>

<div class="row">
    <div class="col-md-8">
		<?= $this->render('view/_info', ['model' => $model]) ?>
		<?= $this->render('view/_comments', ['model' => $model]) ?>
    </div>
	<div class="col-md-4">
		<div class="pull-right">
			<?= $this->render('view/_sidebar', ['model' => $model]) ?>
		</div>
	</div>
</div>

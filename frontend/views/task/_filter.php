<?php

use common\models\TaskStatus;
use common\models\search\TaskSearch;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\Project $project
 * @var integer $typeId
 * @var string $filter
 */

$filters = TaskSearch::getFilters();
$statuses = TaskStatus::getArray();
?>

<div class="btn-group pull-right">
	<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Filters <span class="caret"></span></button>
	<ul class="dropdown-menu" role="menu">
		<?php foreach ($filters as $item): ?>
			<li>
				<?= Html::a(TaskSearch::getFilterName($item), ['task/index',
					'projectId' => $project->id,
					'typeId' => $typeId,
					'filter' => $item
				]) ?>
			</li>
		<?php endforeach ?>
		<li class="divider"></li>
		<?php foreach ($statuses as $key => $value): ?>
			<li>
			<?php
				if ($key != TaskStatus::STATUS_UNRESOLVED && $key != TaskStatus::STATUS_CLOSED) {
					echo Html::a($value, ['task/index',
						'projectId' => $project->id,
						'typeId' => $typeId,
						'statusId' => $key
					]);
				} else {
					echo Html::a($value, ['task/index',
						'projectId' => $project->id,
						'typeId' => $typeId,
						'status' => $key
					]);
				}
			?>
			</li>
		<?php endforeach ?>
	</ul>
</div>

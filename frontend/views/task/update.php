<?php

/**
 * @var yii\web\View $this
 * @var common\models\Task $model
 */

$this->title = $model->project->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['project/index']];
$this->params['breadcrumbs'][] = $this->title;

$header = 'Update ' . $model->type;
?>

<?= $this->render('_form', [
	'model' => $model,
	'projectBuildId' => 0,
	'assigneeId' => 0,
	'header' => $header
]) ?>

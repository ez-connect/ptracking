<?php

use common\models\FoodMenu;
use common\models\Overtime;
use ez\widgets\SwitchButton;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use kartik\widgets\TimePicker;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\Task $task
 */

$model = new Overtime();
$menus = FoodMenu::getArray($task->project->foodStoreId);
?>

<div class="panel-body">
	<?= $form->field($task, 'isOvertime')->widget(SwitchButton::className(), [
		'options' => [
			'id' => 'switch-button-overtime',
			'data-on-text' => 'YES',
			'data-off-text' => 'NO',
			'data-label-text' => 'OT',
		]
	]) ?>
	<div id="form-overtime" style="display:none">
		<?= $form->field($model, 'workDate')->widget(DatePicker::className(), [
			'options' => ['placeholder' => 'Enter date ...'],
			'pluginOptions' => [
				'autoclose' => true,
				'format' => 'yyyy-mm-dd'
			]
		]) ?>
		
		<?= $form->field($model, 'startTime')->widget(TimePicker::className(), [
			'options' => ['placeholder' => 'Enter start time ...'],
			'pluginOptions' => [
				'minuteStep' => 30,
				'showSeconds' => false,
				'showMeridian' => false
			]
		]) ?>
		
		<?= $form->field($model, 'endTime')->widget(TimePicker::className(), [
			'options' => ['placeholder' => 'Enter end time ...'],
			'pluginOptions' => [
				'minuteStep' => 30,
				'showSeconds' => false,
				'showMeridian' => false
			]
		]) ?>
		
		<?php if (count($menus) > 0): ?>
			<?= $form->field($model, 'menuId')->widget(Select2::className(), ['data' => $menus]) ?>
		<?php endif ?>
	</div>
</div>

<?php

/**
 * @var yii\web\View $this
 * @var common\models\Task $model
 * @var integer $projectBuildId
 * @var integer $assigneeId
 */

$this->title = $model->project->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['project/index']];
$this->params['breadcrumbs'][] = $this->title;

$header = 'Create ' . $model->type;
?>

<?= $this->render('_form', [
	'model' => $model,
	'projectBuildId' => $projectBuildId,
	'assigneeId' => $assigneeId,
	'header' => $header
]) ?>

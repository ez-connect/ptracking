<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\BuildSearch $searchModel
 */

$this->title = 'Builds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="build-index">

	<?php echo GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'name',
			[
				'attribute' => 'deviceTypeId',
				'value' => function ($model, $index, $widget) {
					return $model->deviceType->name;
				}
			],
			[
				'attribute' => 'screenSizeId',
				'value' => function ($model, $index, $widget) {
					return $model->screenSize->name;
				}
			],
			[
				'attribute' => 'priorityId',
				'value' => function ($model, $index, $widget) {
					return $model->priority->name;
				}
			],

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>

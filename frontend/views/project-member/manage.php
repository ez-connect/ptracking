<?php

use common\models\Role;
use common\models\User;
use ez\helpers\DateTime;
use ez\widgets\ModalUpdate;
use ez\widgets\LiveFilter;
use ez\widgets\Modal;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\Project $project
 */

$this->title = $project->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['project/index']];
$this->params['breadcrumbs'][] = $this->title;

$users = User::getArrayByProjectId($project->id);
?>

<?= $this->render('//layouts/_navbar', ['project' => $project, 'controller' => 'project-member']) ?>

<?php
// Create a Modal to update the role of a member
Modal::begin([
	'id' => 'popup-modal',
	'clearData' => true,
	'clientOptions' => [
		'modal' => true,
		'autoOpen' => false,
	],
]);
Modal::end();
?>


<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<strong>Manage members</strong>
		<div class="col-sm-3 col-md-3 pull-right">
			<?=	LiveFilter::widget([
				'name' => 'filter',
				'selection' => '.grid-view tbody tr',
				'options' => [
					'placeholder' => 'Search...',
				]
			]);	?>
		</div>

		<?php if (count($users) > 0): ?>
		<?= $this->render('_create', ['project' => $project, 'users' => $users]) ?>
		<?php endif ?>
	</div>
	<div class="panel-body">
		<?php echo GridView::widget([
			'dataProvider' => $dataProvider,
			'tableOptions' => [
				'class' => 'table table-hover',
			],
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],

				[
					'attribute' => 'memberId',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return $model->member->name;
					}
				],
				[
					'attribute' => 'Role',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a(Role::getName($model->member->role), ['update', 'id' => $model->id], [
							'data-toggle' => 'modal',
							'data-target' => '#popup-modal'
						]);
					}
				],
				[
					'attribute' => 'authorId',
					'value' => function($model, $index, $widget) {
						return $model->author->name;
					}
				],
				[
					'attribute' => 'createTime',
					'value' => function($model, $index, $widget) {
						return DateTime::timeAgo($model->createTime);
					}
				],
				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{delete}',
					'buttons' => [
						'delete' => function ($url, $model) {
							return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
								'data-toggle' => 'modal',
								'data-target' => '#popup-modal'
							]);
						}
					],
				],
			],
		]); ?>
	</div>
</div>

<?php

use common\models\Role;
use ez\helpers\DateTime;
use ez\widgets\LiveFilter;
use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var common\models\Project $project
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = $project->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['project/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('//layouts/_navbar', ['project' => $project, 'controller' => 'project-member']) ?>

<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<strong>Members</strong>
		<div class="col-sm-3 col-md-3 pull-right">
		<?=	LiveFilter::widget([
			'name' => 'filter',
			'selection' => '.grid-view tbody tr',
			'options' => [
				'placeholder' => 'Search...',
			]
		]);	?>
		</div>
	</div>
	<div class="panel-body">
		<?php echo GridView::widget([
			'dataProvider' => $dataProvider,
			'tableOptions' => [
				'class' => 'table table-hover',
			],
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],

				[
					'attribute' => 'memberId',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return $model->member->name;
					}
				],
				[
					'attribute' => 'role',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Role::getName($model->member->role);
					}
				],
				[
					'attribute' => 'Assignment',
					'format' => 'raw',
					'value' => function($model, $index, $widget) {
						return Html::a(count($model->assignments) . ' build(s)', ['project-build/index', 
							'projectId' => $model->projectId,
							'projectMemberId' => $model->id
						]);
					}
				],
				[
					'attribute' => 'authorId',
					'value' => function($model, $index, $widget) {
						return $model->author->name;
					}
				],
				[
					'attribute' => 'createTime',
					'value' => function($model, $index, $widget) {
						return DateTime::timeAgo($model->createTime);
					}
				],
			],
		]); ?>
	</div>
</div>

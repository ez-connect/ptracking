<?php

use common\models\Role;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\User $user
 */
?>

<?php $form = ActiveForm::begin() ?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title">Update the <code>role</code> for <strong><?= $user->name ?></strong></h4>
</div>

<div class="modal-body">
	<p>Select a <code>role</code> for your member.</p>
	<?= $form->field($user, 'role')->dropDownList(Role::getArray()) ?>
</div>

<div class="modal-footer">
	<?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

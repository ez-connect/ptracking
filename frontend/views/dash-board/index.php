<?php

use ez\helpers\DateTime;
use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $projectDataProvider
 * @var yii\data\ActiveDataProvider $foodStoreDataProvider
 */
$this->title = 'Dash board';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_project', ['dataProvider' => $projectDataProvider]) ?>
<?= $this->render('_foodStore', ['dataProvider' => $foodStoreDataProvider]) ?>

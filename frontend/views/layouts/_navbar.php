<?php

use common\models\Task;
use common\models\TaskStatus;
use common\models\TaskType;
use yii\bootstrap\Nav;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Project $project
 * @var string $controller
 * @var integer $typeId
 */
?>

<?php
	$projectId = $project->id;
	$tabItems = [
		['label' => '<span class="glyphicon glyphicon-home"></span> Statistics',
			'url' => ['/project/view', 'id' => $projectId],
			'active' => ($controller == 'project')
		],
		['label' => '<span class="glyphicon glyphicon-phone"></span> Build',
			'url' => ['/project-build/index', 'projectId' => $projectId],
			'active' => ($controller == 'project-build')
		],
		['label' => '<span class="glyphicon glyphicon-check"></span> Target',
			'url' => ['/task/index', 'projectId' => $projectId, 'typeId' => TaskType::TYPE_TARGET, 'status' => TaskStatus::STATUS_UNRESOLVED],
			'active' => ($controller == 'task' && $typeId == TaskType::TYPE_TARGET)
		],
		['label' => '<span class="glyphicon glyphicon-question-sign"></span> Issue',
			'url' => ['/task/index', 'projectId' => $projectId, 'typeId' => TaskType::TYPE_ISSUE, 'status' => TaskStatus::STATUS_UNRESOLVED],
			'active' => ($controller == 'task' && $typeId == TaskType::TYPE_ISSUE)
		],
		['label' => '<span class="glyphicon glyphicon-camera"></span> Feedback',
			'url' => ['/task/index', 'projectId' => $projectId, 'typeId' => TaskType::TYPE_FEEDBACK, 'status' => TaskStatus::STATUS_UNRESOLVED],
			'active' => ($controller == 'task' && $typeId == TaskType::TYPE_FEEDBACK)
		],
		['label' => '<span class="glyphicon glyphicon-calendar"></span> Milestone',
			'url' => ['/task/index', 'projectId' => $projectId, 'typeId' => TaskType::TYPE_MILESTONE, 'status' => TaskStatus::STATUS_UNRESOLVED],
			'active' => ($controller == 'task' && $typeId == TaskType::TYPE_MILESTONE)
		],
		['label' => '<span class="glyphicon glyphicon-eye-open"></span> Assigment',
			'url' => ['/project-assignment/index', 'projectId' => $projectId],
			'active' => ($controller == 'project-assignment')
		],
		['label' => '<span class="glyphicon glyphicon-user"></span> Member',
			'url' => ['/project-member/index', 'projectId' => $projectId],
			'active' => ($controller == 'project-member')
		],
		['label' => '<span class="glyphicon glyphicon-pencil"></span> Overtime',
			'url' => ['/overtime/index', 'projectId' => $projectId, 'workDate' => date('Y-m-d')],
			'active' => ($controller == 'overtime')
		],
	];

	echo Nav::widget([
		'options' => ['class' => 'nav nav-tabs'],
		'items' => $tabItems,
		'encodeLabels' => false
	]);
?>
<p></p>

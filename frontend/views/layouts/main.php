<?php

use common\models\Role;
use common\models\Project;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>

<?php
if (!Yii::$app->user->isGuest) {
    $summary = Yii::$app->user->identity->summary;
}
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => 'Project Tracking',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $menuItems = [
                ['label' => 'Home', 'url' => ['/site/index']],
                ['label' => 'Project', 'url' => ['/project']],
                ['label' => 'Contact', 'url' => ['/site/contact']],
                // ['label' => 'About', 'url' => ['/site/about']],
            ];
            if (Yii::$app->user->isGuest) {

                $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } else {
                $menuItems[] = ['label' => "Your assignment", 'url' => ['#'],
                    'items' => [
                        ['label' => "target <span class='badge pull-right'>{$summary['unresolved-target']}</span>", 'url' => ['#']],
                        ['label' => "issue <span class='badge pull-right'>{$summary['unresolved-issue']}</span>", 'url' => ['#']],
                        ['label' => "feedback <span class='badge pull-right'>{$summary['unresolved-feedback']}</span>", 'url' => ['#']],
                        ['label' => "milestone <span class='badge pull-right'>{$summary['unresolved-feedback']}</span>", 'url' => ['#']],
                    ],
                ];

                if (Yii::$app->user->checkAccess(Role::ROLE_MANAGER)) {
                    $menuItems[] = ['label' => 'Dash board', 'url' => ['/dash-board/index']];
                }

                $menuItems[] = ['label' => Yii::$app->user->identity->getAvatarImg(20, 20), 'url' => ['#'],
                    'items' => [
                        ['label' => 'View profile', 'url' => ['#']],
                        ['label' => 'Manage account', 'url' => ['#']],
                        '<li class="divider"></li>',
                        ['label' => 'Logout', 'url' => ['/site/logout']]
                    ],
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
                'encodeLabels' => false
            ]);
            NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>
    <footer class="footer">
        <div class="container">
        <p class="pull-left">
            <?= Html::a('Report issue', 'https://code.ez-connect.net/ptracking/issues', ['target' => '_blank']) ?>
        </p>
        <p class="pull-right">Page generated in <?= round(Yii::getLogger()->getElapsedTime(), 3) ?> seconds</p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

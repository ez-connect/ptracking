<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\Overtime $model
 */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4>Delete overtime record</h4>
</div>
<div class="modal-body">
	Do you want to delete the overtime record for task 
	<code><?= $model->task->name ?></code>?
</div>
<div class="modal-footer">
	<?= Html::submitButton('Yes', ['class' => 'btn btn-default']) ?>
	&nbsp;
	<?= Html::a('No', '#', ['data-dismiss' =>'modal']) ?>
</div>
<?php ActiveForm::end(); ?>

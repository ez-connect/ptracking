<?php

use common\models\FoodMenu;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use kartik\widgets\TimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\Overtime $model
 */

$menus = FoodMenu::getArray($model->project->foodStoreId);
?>

<?php $form = ActiveForm::begin([
	'options' => [
    	'class' => 'form-horizontal',
    ],
    'fieldConfig' => [
		'template' => "{label}\n<div class=\"col-lg-8\">{input}</div>\n<div class=\"col-lg-8 col-md-offset-3\">{error}</div>",
		'labelOptions' => ['class' => 'col-lg-3 control-label'],
    ],
]); ?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4>Update overtime record</h4>
</div>
<div class="modal-body">
<?php
	echo $form->field($model, 'workDate')->widget(DatePicker::className(), [
		'options' => ['placeholder' => 'Enter date ...'],
		'pluginOptions' => [
			'autoclose' => true,
			'format' => 'yyyy-mm-dd'
		]
	]);
	
	echo $form->field($model, 'startTime')->widget(TimePicker::className(), [
		'options' => ['placeholder' => 'Enter start time ...'],
		'pluginOptions' => [
			'minuteStep' => 30,
			'showSeconds' => false,
			'showMeridian' => false
		]
	]);
	
	echo $form->field($model, 'endTime')->widget(TimePicker::className(), [
		'options' => ['placeholder' => 'Enter end time ...'],
		'pluginOptions' => [
			'minuteStep' => 30,
			'showSeconds' => false,
			'showMeridian' => false
		]
	]);
	
	echo $form->field($model, 'menuId')->widget(Select2::className(), ['data' => $menus]);
?>
</div>
<div class="modal-footer">
	<?= Html::submitButton('Update', ['class' => 'btn btn-default']) ?>
	&nbsp;
	<?= Html::a('Cancel', '#', ['data-dismiss' =>'modal']) ?>
</div>
<?php ActiveForm::end(); ?>

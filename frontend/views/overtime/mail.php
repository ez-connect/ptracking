<?php

use common\models\ProjectMember;
use common\models\User;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\models\Project $project
 * @var frontend\models\TaskEmail $model
 */

$toMembers = ProjectMember::getArrayByProjectId($model->project->id);
$ccMembers = User::getArrayByProjectId($model->project->id);
?>

<?php $form = ActiveForm::begin([
	'options' => [
		'class' => 'form-horizontal',
    ],
    'fieldConfig' => [
		'template' => "{label}\n<div class=\"col-lg-10\">{input}</div>\n<div class=\"col-lg-10 col-md-offset-2\">{error}</div>",
		'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]); ?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4>Send email selected overtime records</h4>
</div>
<div class="modal-body">
	<?php
		echo $form->field($model, 'subject')->textInput();
		echo $form->field($model, 'to')->widget(Select2::className(), [
			'data' => $toMembers,
			'options' => [
				'multiple' => true,
			],
		]);
		echo $form->field($model, 'cc')->widget(Select2::className(), [
			'data' => $ccMembers,
			'options' => [
				'multiple' => true,
			],
		]);
		echo $form->field($model, 'body')->textArea(['rows' => 4]);
	?>
</div>
<div class="modal-footer">
	<?= Html::submitButton('Send', ['class' => 'btn btn-default']) ?>
	&nbsp;
	<?= Html::a('Cancel', '#', ['data-dismiss' =>'modal']) ?>
</div>

<?php ActiveForm::end(); ?>

<?php

namespace frontend\controllers;

//use common\models\AccessRule;
use common\models\Overtime;
use common\models\Project;
use common\models\Task;
use common\models\TaskAttachment;
use common\models\TaskComment;
use common\models\TaskStatus;
use common\models\TaskType;
use common\models\Role;
use common\models\search\TaskSearch;
use frontend\models\TaskEmail;
use yii\helpers\BaseJson;
use yii\web\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use Yii;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
//				'ruleConfig' => [
//					'class' => AccessRule::className()
//				],
				'rules' => [
					[
						'allow' => true,
						'actions' => ['index', 'view', 'create', 'change', 'update', 'suggest'],
						'roles' => ['@'],
					],
					[
						'allow' => true,
						'actions' => ['delete', 'mail'],
						'roles' => [Role::ROLE_MANAGER],
					],
				],
			],
		];
	}

	/**
	 * Lists Task models.
	 * @param integer $projectId		project id, require.
	 * @param integer $typeId			task type id, require.
	 * @return mixed
	 */
	public function actionIndex($projectId, $typeId)
	{
		$searchModel = new TaskSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
		
		$filter = Yii::$app->request->get('filter');
		$project = Project::find($projectId);
		
		return $this->render('index', [
			'project' => $project,
			'dataProvider' => $dataProvider,
			'typeId' => $typeId,
			'filter' => $filter,
		]);
	}

	/**
	 * Displays a single Task model.
	 * @param string $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		$model = $this->findModel($id);
		return $this->render('view', [
			'model' => $model,
		]);
	}

	/**
	 * Creates a new Task model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate($projectId, $typeId, $projectBuildId = 0, $assigneeId = 0)
	{
		$model = new Task();
		if ($model->load(Yii::$app->request->post())) {
			// POST -> create one task or many tasks
			$model->projectId = $projectId;
			$model->typeId = $typeId;
			if ($projectBuildId> 0) {
				$model->projectBuildId = $projectBuildId;
			}
			if ($assigneeId> 0) {
				$model->assigneeId = $assigneeId;
			}
			$model->description = Yii::$app->getRequest()->post('Task')['description'];
			// \yii\helpers\VarDumper::dump($model, 10, true);exit();
			
			if ($model->save()) {
				//Attachments
				$shareAttachment = new TaskAttachment();
				if ($shareAttachment->load(Yii::$app->request->post())) {
					$uploadFile = UploadedFile::getInstance($shareAttachment, 'baseName');
					if ($uploadFile != null && $uploadFile->name != null) {
						$attachment = new TaskAttachment();
						$attachment->generateValues($model, $uploadFile);
						$attachment->save();
					}
				}
				
				//Overtime
				if ($model->isOvertime) {
					$overtime = new Overtime();
					if ($overtime->load(Yii::$app->request->post())) {
						$overtime->projectId = $model->projectId;
						$overtime->taskId = $model->id;
						$overtime->save();
					}
				}
			}
			
			Yii::$app->session->setFlash('info', "A new task has been added successful.");
			if ($projectBuildId != 0) {
				return $this->redirect(['project-build/view', 'id' => $projectBuildId]);
			} else {
				return $this->redirect(['view', 'id' => $model->id]);
			}
		}
		else
		{
			// Show input form
			$model = new Task();
			$model->projectId = $projectId;
			$model->typeId = $typeId;
			$model->assigneeId = Yii::$app->user->identity->id;

			return $this->render('create', [
				'model' => $model,
				'projectBuildId' => $projectBuildId,
				'assigneeId' => $assigneeId,
			]);
		}
	}
	
	/**
	 * Updates an existing Task model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param string $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		
		if ($model->load(Yii::$app->request->post())) {
			// \yii\helpers\VarDumper::dump($model, 10, true); exit();
			// Check dirty attributes (before call Task::save() method) & save a comment for changes
			$comment = new TaskComment();
			$comment->load(Yii::$app->request->post());
			$isChanged = $comment->generateValues($model);	//keep logic since use Select2 with multi selections
			
			if ($isChanged && $model->save()) {
				// \yii\helpers\VarDumper::dump($this, 10, true); exit();
				$comment->save();
				
				//Attachments
				$attachment = new TaskAttachment();
				if ($attachment->load(Yii::$app->request->post())) {
					$uploadFile = UploadedFile::getInstance($attachment, 'baseName');
					// \yii\helpers\VarDumper::dump($uploadFile, 10, true); exit();
					if ($uploadFile != null && $uploadFile->name != null) {
						$attachment->generateValues($model, $uploadFile);
						$attachment->save();
					}
				}
				
				Yii::$app->session->setFlash('success', "This task has been updated.");
			}
			
			//Overtime
			if ($model->isOvertime) {
				$overtime = new Overtime();
				if ($overtime->load(Yii::$app->request->post())) {
					$overtime->projectId = $model->projectId;
					$overtime->taskId = $model->id;
					$overtime->save();
				}
				
				Yii::$app->session->setFlash('success', "Overtime has been recorded.");
			}
			
			return $this->redirect(['view', 'id' => $id]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Task model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $projectId		 	project id
	 * @param integer $id 			task id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		if (Yii::$app->getRequest()->getIsPost()) {
			$model->delete();
			Yii::$app->session->setFlash('info', ucfirst(TaskType::getName($model->typeId)) . ' has been deleteted');
			return $this->redirect(['task/index', 'projectId' => $model->projectId, 'typeId' => $model->typeId, 'status' => TaskStatus::STATUS_UNRESOLVED]);
		} else {
			return $this->renderAjax('delete', ['model' => $model]);
		}
	}
	
	/**
	 * Send an email to all members of a project.
	 * AJAX
	 * @param integer $projectId		project id, require.
	 * @param integer $typeId			task type id, require.
	 * @return mixed
	 */
	public function actionMail($projectId, $typeId)
	{
		$project = Project::find($projectId);
		$model = new TaskEmail($project, $typeId);
		
		if ($model->load(Yii::$app->request->post())) {
			$searchModel = new TaskSearch();
			$dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
			
			$model->generateMessage();
			if (Yii::$app->mail->compose('task', ['model' => $model, 'dataProvider' => $dataProvider])
				->setFrom([Yii::$app->params['supportEmail'] => 'Ptracking'])
				->setTo($model->to)
				->setCc($model->cc)
				->setSubject($model->subject)
				->send()
			) {
				Yii::$app->session->setFlash('info', "The email has been sent successful.");
			} else {
				Yii::$app->session->setFlash('error', "The email can not be sent!");
			}
			
			$params = Yii::$app->getRequest()->getQueryParams();
			$url[] = 'index';				// Can't use ArrayHelper::merge() method
			foreach ($params as $key => $value) {
				$url[$key] = $value;
			}
			
			return $this->redirect($url);
		} else {
			return $this->renderAjax('mail', [
				'model' => $model,
				'typeId' => $typeId
			]);
		}
	}

	public function actionSuggest($projectId, $name)
	{
		$models = Task::find()
			->select('name')
			->where(['projectId' => $projectId])
			->andWhere(['like', 'name', $name])
			->limit(10)
			->column();
			
		echo BaseJson::encode($models);
	}
	
	/**
	 * Finds the Task model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param string $id
	 * @return Task the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Task::find($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}

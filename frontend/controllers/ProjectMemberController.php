<?php

namespace frontend\controllers;

use common\models\User;
use common\models\Project;
use common\models\ProjectMember;
use common\models\Role;
use common\models\search\ProjectMemberSearch;
use yii\data\ActiveDataProvider;
use yii\web\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * ProjectMemberController implements the CRUD actions for ProjectMember model.
 */
class ProjectMemberController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'actions' => ['index'],
						'roles' => ['@'],
					],
					[
						'allow' => true,
						'actions' => ['manage', 'create', 'update', 'delete'],
						'roles' => [Role::ROLE_MANAGER],
					],
				],
			],
		];
	}

	/**
	 * Lists ProjectMember models.
	 * @param integer $projectId		project id, require.
	 * @return mixed
	 */
	public function actionIndex($projectId)
	{
		$searchModel = new ProjectMemberSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
		
		return $this->render('index', [
			'project' => Project::find($projectId),
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays all build member of a project, add update + delete.
	 * @param string $projectId project id
	 * @return mixed
	 */
	public function actionManage($projectId)
	{
		$searchModel = new ProjectMemberSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
		
		return $this->render('manage', [
			'project' => Project::find($projectId),
			'dataProvider' => $dataProvider,
		]);
	}

	/**return $this->redirect(['manage', 'projectId' => $projectId]);
	 * Creates a new ProjectMember model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @param integer $pid the project id.
	 * @return mixed
	 */
	public function actionCreate($pid)
	{
		$count = 0;
		
		if (isset($_POST['members'])) {
			$project = Project::find($pid);
			if ($project == null) {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
			
			$members = $_POST['members'];
			
			foreach ($members as $member) {
				$model = new ProjectMember();
				$model->projectId = $pid;
				$model->memberId = $member;
				
				if (!ProjectMember::isExist($model)) {
					$model->save();
					$count++;
				}
			}
		}
		
		Yii::$app->session->setFlash('info', "Added {$count} member(s) successful.");
		return $this->redirect(['manage', 'projectId' => $pid]);
	}
	
	/**
	 * Update the role of a member.
	 * @param integer $id	member id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$user = User::find($model->memberId);
		
		if (Yii::$app->getRequest()->getIsPost()) {
			if ($user->load($_POST)) {
				$user->save();
				Yii::$app->session->setFlash('success', "The role of <code>{$user->name}</code> has been updated.");
			} else {
				Yii::$app->session->setFlash('danger', "Can not update the role.");
			}
			return $this->redirect(['project-member/manage', 'projectId' => $model->project->id]);
		} else {
			return $this->renderAjax('update', ['user' => $user]);
		}
	}

	/**
	 * Deletes some existing ProjectMember models.
	 * If deletion is successful, the browser will be redirected to the 'manage' page.
	 * @param integeger $id		project member id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		
		if (Yii::$app->getRequest()->getIsPost()) {
			$model->delete();
				
			return $this->redirect(['manage', 'projectId' => $model->projectId]);
		} else {
			return $this->renderAjax('delete', [
				'model' => $model
			]);
		}
	}

	/**
	 * Finds the ProjectMember model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param string $id
	 * @return ProjectMember the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public static function findModel($id)
	{
		if (($model = ProjectMember::find($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}

<?php

namespace frontend\controllers;

use common\models\Overtime;
use common\models\Project;
use common\models\search\OvertimeSearch;
use frontend\models\OvertimeEmail;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;

class OvertimeController extends Controller
{
	public function actionIndex($projectId)
	{
		$searchModel = new OvertimeSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
		$project = Project::find($projectId);
		
		return $this->render('index', [
			'project' => $project,
			'dataProvider' => $dataProvider
		]);
	}

	/* public function actionCreate($projectId)
	{
		$model = new Overtime();
		$project = Project::find($projectId);
		
		return $this->renderAjax('create', [
			'model' => $model,
			'project' => $project
		]);
	} */
	
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['overtime/index', 'projectId' => $model->project->id]);
		} else {
			return $this->renderAjax('update', [
				'model' => $model,
			]);
		}
	}
	
	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		if (Yii::$app->request->getIsPost()) {
			$model->delete();
			return $this->redirect(['overtime/index', 'projectId' => $model->project->id]);
		} else {
			return $this->renderAjax('delete', ['model' => $model]);
		}
	}
	
	/**
	 * Send an email to all members of a project.
	 * AJAX
	 * @param integer $projectId		project id, require.
	 * @return mixed
	 */
	public function actionMail($projectId)
	{
		$project = Project::find($projectId);
		$model = new OvertimeEmail($project);
		
		if ($model->load(Yii::$app->request->post())) {
			$searchModel = new OvertimeSearch();
			$dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
			
			$model->generateMessage();
			if (Yii::$app->mail->compose('overtime', ['model' => $model, 'dataProvider' => $dataProvider])
				->setFrom([Yii::$app->params['supportEmail'] => 'Ptracking'])
				->setTo($model->to)
				->setCc($model->cc)
				->setSubject($model->subject)
				->send()
			) {
				Yii::$app->session->setFlash('info', "The email has been sent successful.");
			} else {
				Yii::$app->session->setFlash('error', "The email can not be sent!");
			}
			
			$params = Yii::$app->getRequest()->getQueryParams();
			$url[] = 'index';				// Can't use ArrayHelper::merge() method
			foreach ($params as $key => $value) {
				$url[$key] = $value;
			}
			
			return $this->redirect($url);
		} else {
			return $this->renderAjax('mail', [
				'model' => $model,
			]);
		}
	}
	
	protected function findModel($id)
	{
		if (($model = Overtime::find($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}

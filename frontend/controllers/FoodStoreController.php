<?php

namespace frontend\controllers;

use common\models\FoodStore;
use common\models\Role;
use common\models\search\FoodStoreSearch;
use common\models\search\FoodMenuSearch;
use yii\helpers\ArrayHelper;
use yii\web\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * FoodStoreController implements the CRUD actions for FoodStore model.
 */
class FoodStoreController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'only' => ['create', 'update', 'delete'],
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'actions' => ['create', 'update', 'delete'],
						'roles' => [Role::ROLE_MANAGER],
					],
				],
			],
		];
	}

	/**
	 * Lists all FoodStore models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new FoodStoreSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single FoodStore model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		$searchModel = new FoodMenuSearch();
		$params = Yii::$app->request->getQueryParams();
		$params = ArrayHelper::merge($params, ['foodStoreId' => $id]);
		ArrayHelper::remove($params, 'id');
		$dataProvider = $searchModel->search($params);
		
		return $this->render('view', [
			'model' => $this->findModel($id),
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Creates a new FoodStore model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new FoodStore();
		
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['dash-board/index']);
		} else {
			return $this->renderAjax('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing FoodStore model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['dash-board/index']);
		} else {
			return $this->renderAjax('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing FoodStore model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		if (Yii::$app->request->getIsPost()) {
			$model->delete();
			return $this->redirect(['dash-board/index']);
		} else {
			return $this->renderAjax('delete', ['model' => $model]);
		}
	}

	/**
	 * Finds the FoodStore model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return FoodStore the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if ($id !== null && ($model = FoodStore::find($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}

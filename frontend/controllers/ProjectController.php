<?php

namespace frontend\controllers;

use common\models\Milestone;
use common\models\Project;
use common\models\ProjectAssignment;
use common\models\ProjectBuild;
use common\models\ProjectMember;
use common\models\Task;
use common\models\search\ProjectSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;

class ProjectController extends Controller
{
	/**
	 * Lists all Project models.
	 * @return mixed
	 */
	public function actionIndex($filter = Project::PROJECT_ALL)
	{
		$searchModel = new ProjectSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
		
		return $this->render('index', [
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Project model.
	 * @param string $id project id
	 * @param integer $tab
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
			'action' => 'view',
		]);
	}
	
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['dash-board/index']);
		} else {
			return $this->renderAjax('update', [
				'model' => $model,
			]);
		}
	}
	
	/**
	 * Finds the Task model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param string $id
	 * @return Task the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Project::find($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}

<?php

namespace frontend\controllers;

use common\models\Project;
use common\models\ProjectAssignment;
use common\models\Role;
use common\models\search\ProjectAssignmentSearch;
use yii\data\ActiveDataProvider;
use yii\web\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;

class ProjectAssignmentController extends \yii\web\Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'actions' => ['index', 'view'],
						'roles' => ['@'],
					],
					[
						'allow' => true,
						'actions' => ['manage', 'create', 'update', 'delete'],
						'roles' => [Role::ROLE_MANAGER],
					],
				],
			],
		];
	}
	
	/**
	 * Display ProjectAssignment models.
	 * @param integer $projectId		project id, require.
	 * @return mixed
	 */
	public function actionIndex($projectId)
	{
		$searchModel = new ProjectAssignmentSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
		
		return $this->render('index', [
			'project' => Project::find($projectId),
			'dataProvider' => $dataProvider,
		]);
	}
	
	public function actionManage($projectId)
	{
		$project = Project::find($projectId);
		if ($project == null) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		
		// Assigments
		$searchModel = new ProjectAssignmentSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
		
		return $this->render('manage', [
			'project' => $project,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionCreate($projectId)
	{
		$count = 0;

		if (isset($_POST['members']) && isset($_POST['builds'])) {
			$project = Project::find($projectId);
			if ($project == null) {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
			
			$members = $_POST['members'];
			$projectBuildId = $_POST['builds'][0];
			
			foreach ($members as $member) {
				$model = new ProjectAssignment();
				$model->projectId = $projectId;
				$model->projectMemberId = $member;
				$model->projectBuildId = $projectBuildId;
				
				if (!ProjectAssignment::isExist($model)) {
					$model->save();
					$count++;
				}
			}
		}
		
		Yii::$app->session->setFlash('info', "Assigned {$count} member(s) successful.");
		return $this->redirect(['manage', 'projectId' => $projectId]);
	}
	
	/**
	 * Delete a ProjectAssignment model.
	 * @TODO show the task + other info which relatived to this model for confirmation.
	 */
	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		
		if (Yii::$app->getRequest()->getIsPost()) {
			$model->delete();
			Yii::$app->session->setFlash('info', "The assigment of <code>{$model->projectMember->member->name}</code> has been deleted.");
			return $this->redirect(['manage', 'projectId' => $model->projectId]);
		} else {
			return $this->renderAjax('delete', [
				'model' => $model
			]);
		}
	}
	
	/**
	 * Finds the ProjectAssignment model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param string $id
	 * @return Task the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = ProjectAssignment::find($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}

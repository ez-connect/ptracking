/**
 * Core java script for the application.
 * @author Thanh Vinh <thanh.vinh@hotmail.com>
 * @since 1.0.0
 */

$(document).ready(function() {	
	/**
	 * Handle event when click a change status link
	 * Action: project-build/view -> change
	 */
	$('.build-change-status').click(function() {
		selectedElement = $(this);
		var title = $(this).attr('title');
		$('#change-modal-title').html(title);
		
		var attribute = $(this).attr('data-attribute');
		var name = 'ProjectBuild[' + attribute + ']';
		$('#change-attribute').attr('name', name);
		
		var status = $(this).attr('data-status');
		$('#change-attribute').val(status);
		
		// Show the modal
		$('#change-modal').modal('show');
	});
	
	/**
	 * Handle event when click a change status link
	 * Action task/view -> change
	 */
	$('.task-change').click(function() {
		// Set the title
		var title = $(this).attr('data-title');
		$('#change-modal-title').html(title);
		
		// Set form action
		var action = $('#change-form').attr('action');
		var status = $(this).attr('data-status');
		if (status != null) {
			$('#task-statusid').val(status);
		}
		var isAttachment = $(this).attr('data-attachment');
		if (isAttachment) {
			$('.field-taskattachment-basename').show();
		} else {
			$('.field-taskattachment-basename').hide();
		}
		
		// Custom action in tag
		action = $(this).attr('data-action');
		if (action != null) {
			$('#change-form').attr('action', action);
			$('.field-taskcomment-comment').hide();
		} else {
			$('.field-taskcomment-comment').show();
		}
		
		// Custom submit button label
		var label = $(this).attr('data-submit-label');
		if (label != null) {
			$('#change-primary-btn').html(label);
		} else {
			$('#change-primary-btn').html('Change');
		}
		
		// Show the modal
		$('#change-modal').modal('show');
	});

	/**
	 * Task comment
	 */
	$('#task-comment').click(function() {
		$('#task-comment-modal').modal('show');
	});
	
	/**
	 * Overtime
	 * @TODO change event is called twice
	 */
	$('#switch-button-overtime').change(function(event) {
		event.preventDefault();
		if (this.checked) {
			$('#form-overtime').show();
		} else {
			$('#form-overtime').hide();
		}
	});
	
	/**
	 * Bootstrap tooltip
	 */
	$('.tip').hover(function() {
		$(this).tooltip('show');
	});
	
	/**
	 * Ajax submit form
	 */
	/* jQuery(function($) {
		$(document).on('submit', 'form[data-async]', function(event) {
			var $form = $(this);
			var $target = $($form.attr('data-target'));
			var submitButton = $("input[type='submit'][clicked=true], button[type='submit'][clicked=true]", $form);

			var formData = $form.serializeArray();
			if (submitButton.size() === 1) {
				formData.push({ name: $(submitButton[0]).attr("name"), value: "1" });
			} else if(submitButton.size() !== 0) {
				console.log("Multiple submit-buttons pressed. This should not happen!");
			}

			$.ajax({
				type: $form.attr('method'),
				url: $form.attr('action'),
				data: formData,

				success: function(data, status) {
					$target.html(data);
				}
			});

			event.preventDefault();
		});

		$(document).on("click", 'input[type="submit"], button[type="submit"]', function() {
			$('form[data-async] input[type=submit], form[data-async] button[type=submit]', $(this).parents("form")).removeAttr("clicked");
			$(this).attr("clicked", "true");
		});
	}); */
	
	/**
	 * Google analytic
	 */
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-48347772-1', '128.199.244.111');
	ga('send', 'pageview');

});

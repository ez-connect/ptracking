<?php
namespace frontend\models;

/**
 * Task email form
 */
class OvertimeEmail extends EmailModel
{
	public function __construct($project, $config = [])
	{
		parent::__construct($project, $config);
		
		$this->subject = "[PT][{$this->project->code}] Overtime records";
		
		$this->body = 'Please follow your tasks and don\'t forget to update the status of them before go home.';
	}
}

<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $email;
    public $password;
    public $passwordVerify;
    public $name;
    public $avatar;
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            [['name', 'password', 'passwordVerify'], 'required'],
            [['name', 'password'], 'string', 'min' => 6, 'max' => 50],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            ['name', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This name has already been taken.'],

            ['name', 'match', 'not' => true, 'pattern' => '/[^a-zA-Z0-9_-\s]/', 'message' => 'Unicode character not allowed.'],

            ['avatar', 'image', 'maxWidth' => 128, 'maxHeight' => 128],

            // verify password
            ['passwordVerify', 'compare', 'compareAttribute' => 'password', 'operator' => '==',
                'message'=>'Password not match.'
            ],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'passwordVerify' => 'Retype password',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
}

<?php
namespace frontend\models;

use common\models\TaskType;

/**
 * Task email form
 */
class TaskEmail extends EmailModel
{
	public $typeId;
	
	public function __construct($project, $typeId, $config = [])
	{
		parent::__construct($project, $config);
		
		$this->typeId = $typeId;
		$type = ucfirst(TaskType::getName($this->typeId));
		$this->subject = "[PT][{$this->project->code}] {$type}";
		
		$this->body = 'Please follow your tasks and don\'t forget to update the status of them at the end of each day.';
	}
}

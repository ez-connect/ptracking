<?php
namespace frontend\models;

use common\models\ProjectMember;
use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Task email form
 */
class EmailModel extends Model
{
	public $project;
	public $typeId;
	public $subject;
	public $from;
	public $to;
	public $cc;
	public $body;
	
	public function __construct($project, $config = [])
	{
		parent::__construct($config);
		
		$this->project = $project;
		$this->from = Yii::$app->user->identity;
		$members = ProjectMember::getArrayByProjectId($project->id);
		foreach ($members as $key => $value) {
			$this->to[] = $key;
		}
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['subject', 'body', 'to'], 'required'],
			[['cc'], 'safe'],
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'subject' => 'Subject',
			'to' => 'To',
			'cc' => 'Cc',
			'body' => 'Body',
		];
	}
	
	/**
	 * Adjustment subject, to, cc fields.
	 */
	public function generateMessage()
	{
		$members = User::find()
			->where(['id' => $this->to])
			->all();
		$this->to = [];
		foreach ($members as $member) {
			$this->to[$member->email] = $member->name;
		}
		
		$members = User::find()
			->where(['id' => $this->cc])
			->all();
		$this->cc = [];
		foreach ($members as $member) {
			$this->cc[$member->email] = $member->name;
		}
		
		$this->body = str_replace(PHP_EOL, '<br />', $this->body);
	}
}

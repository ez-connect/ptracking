Ptracking application
===================================

Ptracking is a simple Yii 2 application for project manager.

It includes member manager, assignment manager, build manager and task manager.


DIRECTORY STRUCTURE
-------------------

```
common
	config/             contains shared configurations
	models/             contains model classes used in both backend and frontend
console
	config/             contains console configurations
	controllers/        contains console controllers (commands)
	migrations/         contains database migrations
	models/             contains console-specific model classes
	runtime/            contains files generated during runtime
backend
	assets/             contains application assets such as JavaScript and CSS
	config/             contains backend configurations
	controllers/        contains Web controller classes
	models/             contains backend-specific model classes
	runtime/            contains files generated during runtime
	views/              contains view files for the Web application
	web/                contains the entry script and Web resources
frontend
	assets/             contains application assets such as JavaScript and CSS
	config/             contains frontend configurations
	controllers/        contains Web controller classes
	models/             contains frontend-specific model classes
	runtime/            contains files generated during runtime
	views/              contains view files for the Web application
	web/                contains the entry script and Web resources
vendor/                 contains dependent 3rd-party packages
environments/                contains environment-based overrides
```


REQUIREMENTS
------------

The minimum requirement by this application template that your Web server supports PHP 5.4.0.


INSTALLATION
------------

### Install from an Archive File

Extract the archive file downloaded from [Bitbucket](https://bitbucket.org/thanh-vinh/ptracking/) to
a directory named `ptracking` that is directly under the Web root.

Then follow the instructions given in "GETTING STARTED".


### Install via Composer

TBC

~~~
php composer.phar create-project --stability=alpha thanh-vinh/ptracking
~~~


GETTING STARTED
---------------

After you install the application, you have to conduct the following steps to initialize
the installed application. You only need to do these once for all.

1. Run command `init` to initialize the application with a specific environment.
2. Create a new database and adjust the `components.db` configuration in `common/config/params.php` accordingly.
3. Import `setup/schema.sql` to create DB structure.
	`mysql --user=*<user_name>* --password *<database_name>* < setup/schema.sql`
4. Import `setup/dump.sql` to initialize the sample records.
	`mysql --user=*<user_name>* --password *<database_name>* < setup/dump.sql`
5. Import `setup/rbac.sql` to create RBAC structure.
	`mysql --user=*<user_name>* --password *<database_name>* < rbac/dump.sql`
6. Run command `yii rbac/setup` to create the roles.
7. Update all require packages: `php composer.phar update`.

Now you should be able to access:

- the frontend using the URL `http://localhost/advanced/frontend/web/`
- the backend using the URL `http://localhost/advanced/backend/web/`
